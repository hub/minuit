// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) Andrii Zymohliad, 2022-2023 Hubert Figuière
// Originally copied from https://gitlab.com/azymohliad/qwertone/-/blob/master/src/audio/sources/mixer.rs

use crossbeam_channel::{self, Receiver, Sender};

use super::AudioSource;

const CHANNEL_SIZE: usize = 256;

/// An input source.
struct Input {
    id: usize,
    source: Box<dyn AudioSource + Send>,
    enabled: bool,
    volume: f32,
}

/// Message sent to the [`Mixer`]
pub enum Message {
    _SetInputEnabled((usize, bool)),
    /// Replace current synth with (synth, volume)
    ReplaceSynth((Box<dyn AudioSource + Send>, f32)),
    _SetInputVolume((usize, f32)),
}

pub type MixerHandle = Sender<Message>;

/// Mixer for sound inputs.
/// This will be used as the fixed output for the [Audio Engine][super::engine::Engine]
pub struct Mixer {
    inputs: Vec<Input>,
    messages: Receiver<Message>,
    synth_id: Option<usize>,
    last_id: usize,
    buffer: [[f32; super::DEFAULT_RATE as usize]; 2],
}

impl Mixer {
    pub fn new() -> (Mixer, MixerHandle) {
        let (tx, rx) = crossbeam_channel::bounded(CHANNEL_SIZE);
        let mixer = Mixer {
            inputs: Vec::new(),
            messages: rx,
            synth_id: None,
            last_id: 1,
            buffer: [[0.0_f32; super::DEFAULT_RATE as usize]; 2],
        };
        (mixer, tx)
    }

    /// Add a source to the mixer. Return the last_id.
    /// last_id increment each time.
    pub fn add(
        &mut self,
        source: Box<dyn AudioSource + Send>,
        enabled: bool,
        volume: f32,
    ) -> usize {
        let volume = volume.clamp(0.0, 1.0);
        let current_id = self.last_id;
        let input = Input {
            id: current_id,
            source,
            enabled,
            volume,
        };
        self.last_id += 1;
        self.inputs.push(input);
        current_id
    }

    /// Remove the source by `source_id`.
    ///
    /// Returns `true` if it was found.
    pub fn remove(&mut self, source_id: usize) -> bool {
        if let Some(idx) = self.inputs.iter().position(|i| i.id == source_id) {
            let _input = self.inputs.remove(idx);
            return true;
        }
        false
    }
}

impl AudioSource for Mixer {
    fn get_samples(&mut self, n_frames: usize) -> [&[f32]; 2] {
        let mut num_inputs = 0;
        self.buffer.iter_mut().for_each(|ch| {
            ch.fill(0.0);
        });
        let buffer = &mut self.buffer;
        self.inputs.iter_mut().filter(|x| x.enabled).for_each(|x| {
            num_inputs += 1;
            let volume = x.volume;
            x.source
                .get_samples(n_frames)
                .iter()
                .enumerate()
                .for_each(|(idx, ch)| {
                    let out_ch = &mut buffer[idx];
                    ch.iter()
                        .map(|v| v * volume)
                        .enumerate()
                        .for_each(|(idx, v)| out_ch[idx] += v)
                })
        });
        self.buffer.iter_mut().for_each(|ch| {
            let _ = ch.iter_mut().map(|v| *v / num_inputs as f32);
        });
        [&self.buffer[0], &self.buffer[1]]
    }

    fn process_messages(&mut self) {
        loop {
            match self.messages.try_recv() {
                Ok(Message::_SetInputEnabled((idx, enabled))) => {
                    self.inputs.get_mut(idx).unwrap().enabled = enabled;
                }
                Ok(Message::ReplaceSynth((synth, volume))) => {
                    if let Some(synth_id) = self.synth_id {
                        self.remove(synth_id);
                    }
                    self.synth_id = Some(self.add(synth, true, volume));
                }
                Ok(Message::_SetInputVolume((idx, volume))) => {
                    self.inputs.get_mut(idx).unwrap().volume = volume;
                }

                Err(_) => break,
            }
        }

        for input in self.inputs.iter_mut() {
            input.source.process_messages();
        }
    }
}
