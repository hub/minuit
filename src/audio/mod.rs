// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2020-2023 Hubert Figuière
//

mod engine;
mod mixer;

pub use engine::Engine;
pub use mixer::Message as MixerMessage;
pub use mixer::{Mixer, MixerHandle};

pub const DEFAULT_RATE: u32 = 48000;
pub const DEFAULT_CHANNELS: usize = 2;

/// The trait for the AudioSource.
/// Implement that trait to output sound.
pub trait AudioSource {
    /// Get the next sample.
    fn get_samples(&mut self, n_frames: usize) -> [&[f32]; 2];
    /// Process messages.
    ///
    /// This is where you want to run the sound generator after processing the input.
    fn process_messages(&mut self);
}
