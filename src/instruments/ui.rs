// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2020-2023 Hubert Figuière
//

/// Instrument UI trait
pub trait InstrumentUi: Send + Sync {
    /// Setup the interface / callbacks, return the widget created.
    fn setup(&mut self, ui: &str) -> Option<gtk4::Widget>;
}

/// Get the builder from the UI file. Call this in setup if you need to get a builder.
pub fn get_builder(ui: &str) -> gtk4::Builder {
    gtk4::Builder::from_resource(&format!("/net/figuiere/compiano/instruments/{ui}.ui"))
}
