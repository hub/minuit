// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2020-2023 Hubert Figuière
//

//! The instruments.
//!
#![doc = include_str!("../../doc/instruments.md")]

pub mod fluidlite_ui;
mod instrument;
mod param_value;
pub mod ui;

use std::collections::HashMap;
use std::sync::Mutex;

use gtk4::gio;
use once_cell::sync::Lazy;
use serde_derive::Deserialize;

pub use instrument::Instrument;
pub use instrument::State as InstrumentState;
pub use param_value::ParamValue;
pub use ui::InstrumentUi;

/// Free form parameters for the `Preset`. The key is a key in the toml.
type Params = HashMap<String, ParamValue>;

#[derive(Clone, Debug, Default, Deserialize)]
/// Instrument preset a set of parameters applied to create an `Instrument`.
pub struct Preset {
    /// ID of preset.
    id: String,
    /// User visible name for preset.
    pub name: String,
    /// Name of synth to instantiate.
    pub synth: String,
    /// UI file
    pub ui: Option<String>,
    /// `Soundbank` name
    pub soundbank: Option<String>,
    /// Params for the synth.
    pub params: Option<Params>,
}

impl Preset {}

/// A list of [`Preset`], key is the `id`.
pub type PresetList = HashMap<String, Preset>;

/// Load the [preset list][PresetList] from the `data`.
fn load_preset_list<B>(data: B) -> PresetList
where
    B: AsRef<str>,
{
    match toml::from_str::<PresetList>(data.as_ref()) {
        Ok(list) => list,
        Err(err) => {
            eprintln!("Error parsing: {err}");
            HashMap::new()
        }
    }
}

/// Global preset registry. Is a [`PresetList`] loaded from the resources.
pub static FACTORY_PRESETS: Lazy<Mutex<PresetList>> = Lazy::new(|| {
    let bytes = gio::resources_lookup_data(
        "/net/figuiere/compiano/instruments/presets.toml",
        gio::ResourceLookupFlags::NONE,
    )
    .expect("Can't load preset list from resources");
    Mutex::new(load_preset_list(String::from_utf8(bytes.to_vec()).unwrap()))
});

#[cfg(test)]
mod tests {
    use super::ParamValue;

    #[test]
    fn test_instruments_loading() {
        let list = super::load_preset_list(include_str!("tests/presets.toml"));
        assert!(!list.is_empty());
        assert_eq!(list.len(), 6);

        let pipeorgan = list.get("pipeorgan");
        assert!(pipeorgan.is_some());
        let pipeorgan = pipeorgan.unwrap();
        assert!(pipeorgan.soundbank.is_some());
        let soundbank = pipeorgan.soundbank.as_ref().unwrap();
        assert_eq!(soundbank, "aegeansymphonic25");
        assert!(pipeorgan.params.is_some());
        let params = pipeorgan.params.as_ref().unwrap();
        let soundfont = params.get("soundfont");
        assert!(soundfont.is_some());
        let soundfont = soundfont.unwrap();
        assert_eq!(
            soundfont,
            &ParamValue::Str("AegeanSymphonicOrchestra-v2_5-universal.sf2".to_string())
        );
    }
}
