// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2020-2024 Hubert Figuière
//

//! Implement the UI for the [FluidLite synth][fluidlite_synth::FluidLiteSynth].

use gtk4::glib;
use gtk4::prelude::*;

use crate::settings;
use crate::synth::fluidlite_synth;
use crate::widgets::file_chooser_button::FileChooserButton;

static SETTINGS_FONT_SELECTED: &str = "fluidlite-font-selected";
static SETTINGS_PROGRAM_SELECTED: &str = "fluidlite-program-selected";

/// UI message.
pub enum Message {
    /// Here is the list of [`Banks`][fluidlite_synth::Banks] that are in the soundfont.
    Banks(fluidlite_synth::Banks),
}

/// FluidLite UI
pub struct FluidLiteUi {
    /// The [`FluidLiteSynth`][fluidlite_synth::FluidLiteSynth] to control.
    handle: fluidlite_synth::FluidLiteSynthHandle,
    /// UI messages received.
    receiver: Option<async_channel::Receiver<Message>>,
}

impl FluidLiteUi {
    /// Create a `FluidLiteUi`.
    ///
    /// `receiver` is a receiver of [`Message`].
    ///
    /// `handle` is the handle for the synth, to send commands.
    pub fn new(
        _ui: &str,
        receiver: async_channel::Receiver<Message>,
        handle: &fluidlite_synth::FluidLiteSynthHandle,
    ) -> Self {
        FluidLiteUi {
            handle: handle.clone(),
            receiver: Some(receiver),
        }
    }
}

impl super::InstrumentUi for FluidLiteUi {
    fn setup(&mut self, ui: &str) -> Option<gtk4::Widget> {
        let builder = super::ui::get_builder(ui);
        let w = builder.object::<gtk4::Widget>("instrument");

        if let Some(chooser) = builder.object::<FileChooserButton>("soundfont_choice") {
            let sender = self.handle.0.clone();
            chooser.connect_local(
                "file-set",
                true,
                glib::clone!(
                    #[weak]
                    chooser,
                    #[upgrade_or]
                    None,
                    move |_| {
                        let file = chooser.get_filename();
                        let changed = if let Some(file) = file.as_ref() {
                            settings::set_string_value_if_changed(
                                SETTINGS_FONT_SELECTED,
                                &file.to_string_lossy(),
                            )
                        } else {
                            settings::set_string_value(SETTINGS_FONT_SELECTED, "");
                            true
                        };
                        if changed {
                            settings::set_uint_value(SETTINGS_PROGRAM_SELECTED, 0);
                        }
                        print_on_err!(sender.send(fluidlite_synth::Message::LoadSoundFont(file)));
                        None
                    }
                ),
            );
            let last_soundfont = settings::get_string_value(SETTINGS_FONT_SELECTED);
            if !last_soundfont.is_empty() {
                chooser.set_filename(std::path::PathBuf::from(&last_soundfont));
                chooser.emit_by_name::<()>("file-set", &[]);
            }
        }

        if let Some(program_list) = builder.object::<gtk4::TreeView>("program_list") {
            let column = gtk4::TreeViewColumn::new();
            let cell = gtk4::CellRendererText::new();
            column.set_sizing(gtk4::TreeViewColumnSizing::Fixed);
            column.pack_start(&cell, true);
            column.add_attribute(&cell, "text", 0);
            program_list.append_column(&column);

            let sender = self.handle.0.clone();
            program_list.connect_row_activated(move |w, path, _| {
                if let Some(model) = w.model() {
                    if let Some(iter) = model.iter(path) {
                        let value = model.get_value(&iter, 1);

                        if let Ok(program) = value.get::<u32>() {
                            let bank = ((program & 0xff00) >> 8) as u8;
                            let preset = (program & 0xff) as u8;
                            settings::set_uint_value(SETTINGS_PROGRAM_SELECTED, program);
                            print_on_err!(
                                sender.send(fluidlite_synth::Message::ProgramChange(bank, preset))
                            );
                        }
                    }
                }
            });

            if let Some(receiver) = self.receiver.take() {
                glib::spawn_future_local(glib::clone!(
                    #[weak]
                    program_list,
                    async move {
                        while let Ok(message) = receiver.recv().await {
                            match message {
                                Message::Banks(banks) => {
                                    // populate program list
                                    let model = program_list
                                        .model()
                                        .or_else(|| {
                                            // The tree store contain the bank and preset encoded into column 1
                                            // bank is bits 8-15, preset is 0-8 (actually 7 bits each)
                                            let treestore = gtk4::ListStore::new(&[
                                                glib::Type::STRING,
                                                glib::Type::U32,
                                            ]);
                                            program_list.set_model(Some(&treestore));

                                            program_list.model()
                                        })
                                        .map(|m| m.downcast::<gtk4::ListStore>().unwrap())
                                        .unwrap();
                                    model.clear();

                                    let mut preselected: Option<gtk4::TreeIter> = None;
                                    let selected_program =
                                        settings::get_uint_value(SETTINGS_PROGRAM_SELECTED);
                                    for bank in banks.banks {
                                        for preset in bank.1 {
                                            let iter = model.append();
                                            let program: u32 =
                                                ((bank.0 as u32) << 8) + preset.0 as u32;
                                            if program == selected_program {
                                                preselected = Some(iter);
                                            }
                                            let line = format!(
                                                "{:03}-{:03} {}",
                                                bank.0, preset.0, preset.1
                                            );
                                            model.set(&iter, &[(0, &line), (1, &program)]);
                                        }
                                    }
                                    let col = program_list.column(0).unwrap();
                                    if preselected.is_none() {
                                        preselected = model.iter_first();
                                    }
                                    if let Some(path) = preselected.map(|iter| model.path(&iter)) {
                                        program_list.selection().select_path(&path);
                                        program_list.row_activated(&path, Some(&col));
                                        program_list.scroll_to_cell(
                                            Some(&path),
                                            Some(&col),
                                            false,
                                            0.0,
                                            0.0,
                                        );
                                    }
                                }
                            }
                        }
                    }
                ));
            }
        }

        w
    }
}
