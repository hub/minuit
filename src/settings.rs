// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2020-2023 Hubert Figuière
//

//! Provide high-level primitives to access key values stored in settings.

use gtk4::gio;
use gtk4::gio::prelude::*;

use crate::config;

/// The last MIDI input device.
pub static MIDI_INPUT_DEVICE: &str = "midi-input-device";
/// The MIDI channel for input.
pub static MIDI_INPUT_CHANNEL: &str = "midi-input-channel";
/// The last selected instrument.
pub static SELECTED_INSTRUMENT: &str = "selected-instrument";

/// Create the [`gio::Settings`] used by the application.
/// It will load the schema from installed location if needed.
/// If it can't find it we panic. Glib can't cope with it anyway.
pub fn settings_source() -> gio::Settings {
    let mut path = std::path::PathBuf::from(config::DATADIR);
    path.push("glib-2.0");
    path.push("schemas");

    let schema_source = gio::SettingsSchemaSource::from_directory(
        path,
        gio::SettingsSchemaSource::default().as_ref(),
        true,
    )
    .expect("Coudln't load schema source");
    let schema = schema_source
        .lookup(config::APP_ID, true)
        .expect("Couldn't find schema");
    gio::Settings::new_full(&schema, gio::SettingsBackend::NONE, None)
}

/// Get a string `key` value.
pub fn get_string_value(key: &str) -> String {
    settings_source().string(key).as_str().to_owned()
}

/// Set a string `key` to `value`.
pub fn set_string_value(key: &str, value: &str) {
    print_on_err!(settings_source().set_string(key, value));
}

/// Set the string `key` to `value` only if different.
pub fn set_string_value_if_changed(key: &str, value: &str) -> bool {
    let old_value = get_string_value(key);
    if old_value == value {
        return false;
    }

    print_on_err!(settings_source().set_string(key, value));
    true
}

/// Get an unsigned `key` value.
pub fn get_uint_value(key: &str) -> u32 {
    settings_source().uint(key)
}

/// Set an unsigned integer `key` to `value`.
pub fn set_uint_value(key: &str, value: u32) {
    print_on_err!(settings_source().set_uint(key, value));
}
