// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2020-2024 Hubert Figuière
//

use std::rc::Rc;

extern crate gtk4;
#[macro_use]
extern crate gtk_macros;
extern crate libadwaita as adw;
extern crate midi_control;

use adw::prelude::*;
use gettextrs::*;
use gtk4::gio;
use gtk4::glib;

#[macro_use]
mod utils;

mod audio;
mod audio_stack;
mod config;
mod events;
mod instruments;
mod midi;
mod settings;
mod soundbanks;
mod synth;
mod toolkit;
mod ui;
mod widgets;
mod window;

pub use crate::audio_stack::AudioMessage;

fn main() {
    env_logger::init();

    gtk4::init().unwrap_or_else(|_| panic!("Failed to initialize GTK."));
    widgets::init();

    bindtextdomain(config::GETTEXT_PACKAGE, config::LOCALEDIR).expect("bindtextdomain failed");
    bind_textdomain_codeset(config::GETTEXT_PACKAGE, "UTF-8")
        .expect("bind textdomain codeset failed");
    textdomain(config::GETTEXT_PACKAGE).expect("textdomain failed");

    let res = gio::Resource::load(config::PKGDATADIR.to_owned() + "/compiano.gresource")
        .expect("Could not load resources");
    gio::resources_register(&res);

    if let Some(ref display) = gdk4::Display::default() {
        let icon_theme = gtk4::IconTheme::for_display(display);
        icon_theme.add_resource_path("/net/figuiere/compiano/icons");
    }

    let app = adw::Application::new(Some(config::APP_ID), Default::default());
    app.connect_activate(move |app| {
        let (tx, rx) = async_channel::unbounded();
        events::SENDER.set(tx.clone()).expect("tx already set");

        action!(app, "about", move |_, _| {
            let about = adw::AboutWindow::builder()
                .license_type(gtk4::License::Gpl30)
                .application_icon(config::APP_ID)
                .developers(vec!["Hubert Figuière".to_string()])
                .copyright("©Copyright 2022")
                .name("Compiano")
                .version(config::VERSION)
                .build();
            about.show();
        });
        action!(
            app,
            "quit",
            glib::clone!(
                #[weak]
                app,
                move |_, _| {
                    app.quit();
                }
            )
        );

        app.set_accels_for_action("app.quit", &["<Ctrl>Q"]);
        app.set_accels_for_action("window.close", &["<Ctrl>W"]);
        app.set_accels_for_action("win.preferences", &["<Ctrl>comma"]);

        let window = Rc::new(window::WindowController::new(app, tx));

        let window2 = window.clone();
        let event_handler = async move {
            while let Ok(event) = rx.recv().await {
                events::dispatch(event, &window2);
            }
        };

        toolkit::utils::spawn_local(event_handler);
        window.w.present();
    });

    let ret = app.run();
    std::process::exit(ret.into());
}
