// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2022 Hubert Figuière
//

use std::rc::Rc;

/// `DialogController` helps wrapping a dialog with the control logic
/// It is meant to be implemented by your own code.
///
/// The controller object will be added to the
/// [`WindowController`][crate::window::WindowController].
pub trait DialogController {
    /// Build the widget
    fn build(&mut self);
    /// Show the dialog attached to parent.
    fn show(&self, parent: &gtk4::Window, on_close: Box<dyn Fn()>);
}

/// Create a new dialog for controller `T`. `on_close` will be called
/// when the dialog closes.
pub fn new_dialog<T>() -> Rc<T>
where
    T: DialogController + Default,
{
    let mut controller = T::default();
    controller.build();
    Rc::new(controller)
}
