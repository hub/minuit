// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2021-2024 Hubert Figuière
//

pub mod dialog_controller;
pub mod preferences;

pub use dialog_controller::DialogController;

use crate::events;
use crate::toolkit;

/// Display a message in the UI from the main thread. This expect things to be setup properly.
pub fn display_message_local(msg: String) {
    if let Some(sender) = events::SENDER.get().cloned() {
        toolkit::utils::send_async_local!(events::Message::DisplayMsg(msg), sender);
    }
}

/// Display a message in the UI from any thread.
pub fn display_message_async(msg: String) {
    if let Some(sender) = events::SENDER.get().cloned() {
        toolkit::utils::send_async_any!(events::Message::DisplayMsg(msg), sender);
    }
}
