// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2022-2024 Hubert Figuière
//

use std::cell::RefCell;
use std::rc::Rc;

use adw::prelude::*;
use gtk4;
use gtk4::gio;
use gtk4::glib;
use i18n_format::i18n_fmt;

use midi_control as midi;

use super::dialog_controller::DialogController;
use crate::events;
use crate::midi::MidiState;
use crate::settings;
use crate::toolkit;

#[derive(Default)]
pub struct PreferenceDialog {
    dialog: adw::PreferencesWindow,

    midi_input_combo: adw::ComboRow,
    midi_channel_combo: adw::ComboRow,
}

impl PreferenceDialog {
    /// Setup the dialog
    pub(crate) fn setup(
        &self,
        sender: async_channel::Sender<events::Message>,
        midi_state: Rc<RefCell<MidiState>>,
        settings: &gio::Settings,
    ) {
        let default_device = settings.string(settings::MIDI_INPUT_DEVICE);
        let mut default_id = 0;
        let model = gtk4::StringList::new(&[]);
        for (i, port_name) in MidiState::list_port_names().into_iter().enumerate() {
            model.append(&port_name);
            if default_device == port_name {
                default_id = i;
            }
        }
        self.midi_input_combo.set_model(Some(&model));

        self.midi_input_combo.connect_selected_notify(glib::clone!(
            #[strong]
            sender,
            #[weak]
            midi_state,
            move |w| {
                midi_state.borrow_mut().close();

                let idx = w.selected() as usize;
                let port_name = midi_state.borrow().port_name_at(idx);
                println!("Port name at index {} {:?}", idx, &port_name);
                if let Some(port_name) = port_name {
                    let sender = sender.clone();
                    toolkit::utils::send_async_local!(
                        events::Message::DeviceSelected(port_name),
                        sender
                    );
                }
            }
        ));
        self.midi_input_combo.set_selected(default_id as u32);

        let default_channel =
            midi::Channel::from(settings.uint(settings::MIDI_INPUT_CHANNEL) as u8);
        // XXX make this part of the UI file when Cambalache supports it.
        let model = gtk4::StringList::new(&["Any"]);
        for i in 1..=16 {
            let label = i18n_fmt! {
                // Translators: "Channel" is a MIDI Channel, and "{}" will be replaced by
                // its number. Do not remove.
                i18n_fmt("Channel {}", i)
            };
            model.append(&label);
        }
        self.midi_channel_combo.set_model(Some(&model));

        self.midi_channel_combo
            .connect_selected_notify(glib::clone!(
                #[strong]
                sender,
                move |w| {
                    let ch = w.selected();
                    // The list is 0 index with the Omni mode in 0.
                    let midi_channel = match ch {
                        1..=16 => midi::Channel::from(ch as u8 - 1),
                        _ => midi::Channel::Invalid,
                    };
                    let sender = sender.clone();
                    toolkit::utils::send_async_local!(
                        events::Message::ChannelSelected(midi_channel),
                        sender
                    );
                }
            ));
        self.midi_channel_combo.set_selected(match default_channel {
            midi::Channel::Invalid => 0_u32,
            _ => (default_channel as u8 + 1) as u32,
        });
    }
}

impl DialogController for PreferenceDialog {
    fn build(&mut self) {
        let builder = gtk4::Builder::from_resource("/net/figuiere/compiano/ui/preferences.ui");

        get_widget!(builder, adw::PreferencesWindow, dialog);
        self.dialog = dialog;
        get_widget!(builder, adw::ComboRow, midi_input_combo);
        get_widget!(builder, adw::ComboRow, midi_channel_combo);
        get_widget!(builder, gtk4::Button, open_soundbanks_location);

        open_soundbanks_location.connect_clicked(|_| {
            // Open the soundbank directector
            let p = crate::soundbanks::DownloadManager::get_soundbank_dir();
            if let Ok(uri) = glib::filename_to_uri(p, None) {
                print_on_err!(crate::toolkit::filemanager::open_folder(&uri));
            }
        });

        get_widget!(builder, adw::ActionRow, open_soundbank_row);
        let p = crate::soundbanks::DownloadManager::get_soundbank_dir();
        open_soundbank_row.set_subtitle(p.to_str().unwrap_or(""));

        self.midi_input_combo = midi_input_combo;
        self.midi_channel_combo = midi_channel_combo;
    }

    fn show(&self, parent: &gtk4::Window, on_close: Box<dyn Fn()>) {
        self.dialog.connect_close_request(move |_| {
            on_close();
            glib::Propagation::Proceed
        });
        self.dialog.set_transient_for(Some(parent));
        self.dialog.present();
    }
}
