// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2020-2024 Hubert Figuière
//

pub mod fluidlite_synth;
mod qwertone;
pub mod qwertone_synth;

use std::collections::HashMap;
use std::sync::Mutex;

use midi_control as midi;
use once_cell::sync::Lazy;

use self::fluidlite_synth::FluidLiteSynth;
use self::qwertone_synth::QwertoneSynth;
use crate::audio::AudioSource;
use crate::instruments;
use crate::instruments::InstrumentUi;

/// Error instanciating synth
#[derive(Debug, thiserror::Error)]
pub enum Error {
    /// Initialization failed, with reason
    #[error("Initialization failed: {0}")]
    InitializationFailed(&'static str),
}

pub type Result<T> = std::result::Result<T, Error>;

/// Plugin synth. This is used by a preset as an engine.
///
/// The `handle` is used to send control the synth.
///
/// `ui` is an optional UI displayed.
pub struct Plugin {
    /// Plugin UI
    pub ui: Option<Box<dyn InstrumentUi>>,
    /// Synthesizer
    pub synth: BoxedSynth,
    /// Synthesizer control handle
    pub handle: BoxedHandle,
}

impl Plugin {
    /// New plugin without UI
    pub fn new(synth: BoxedSynth, handle: BoxedHandle) -> Plugin {
        Plugin {
            ui: None,
            synth,
            handle,
        }
    }
    /// New plugin with UI
    pub fn with_ui(ui: Box<dyn InstrumentUi>, synth: BoxedSynth, handle: BoxedHandle) -> Plugin {
        Plugin {
            ui: Some(ui),
            synth,
            handle,
        }
    }
}

/// Boxed Synth handle with the right traits.
pub type BoxedHandle = Box<dyn SynthHandle + Send + Sync>;

/// Handle trait for synthesizer
pub trait SynthHandle {
    /// Load synthesizer.
    /// This should be called when the instrument becomes ready.
    fn load(&self);
    /// Note on command
    fn note_on(&self, event: &midi::KeyEvent);
    /// Note off command
    fn note_off(&self, event: &midi::KeyEvent);
    /// Pitch bend
    /// @value is the MIDI value. 0x2000 being no pitch bend.
    fn pitch_bend(&self, value: u16);

    /// Return the name of the synthesizer
    fn get_name(&self) -> String;
    /// Indicate of the synth support velocity
    fn is_velocity_capable(&self) -> bool;

    /// Let the synth know that a (downloaded) soundbank is available
    fn soundbank_available(&self, soundbank: &str, p: &std::path::Path);
}

/// Boxed Synth
pub type BoxedSynth = Box<dyn AudioSource + Send>;

/// Function to create a Synth.
pub type SynthFactory = fn(u32, &instruments::Preset) -> Result<Plugin>;

/// The registry for Synth. Key is the synth ID.
///
/// Add any new plugin facotory.
pub static SYNTH_REGISTRY: Lazy<Mutex<HashMap<String, SynthFactory>>> = Lazy::new(|| {
    let mut m = HashMap::new();
    m.insert(
        "qwertone".to_string(),
        QwertoneSynth::create as SynthFactory,
    );
    m.insert(
        "fluidlite".to_string(),
        FluidLiteSynth::create as SynthFactory,
    );
    Mutex::new(m)
});
