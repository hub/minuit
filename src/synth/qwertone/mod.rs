// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) Andrii Zymohliad, (c) 2020-2023 Hubert Figuière
// Originally copied from https://gitlab.com/azymohliad/qwertone/-/blob/master/src/audio/sources/synthesizer/mod.rs
//
// Ported to be compatible with pipewire.

mod amplitude;
mod tone;
mod utils;

use std::cell::RefCell;

use crossbeam_channel::{self, Receiver, Sender};

use crate::audio::AudioSource;
use amplitude::AmplitudeMap;
use tone::Tone;

const CHANNEL_SIZE: usize = 256;

pub enum Message {
    NoteHold(usize),
    NoteRelease(usize),
}

pub type SynthesizerHandle = Sender<Message>;

struct State {
    // State
    notes: Vec<Tone>,
    position: u32,
    obertones: Vec<(f32, f32)>,
    active_notes: Vec<usize>,
}

pub struct Synthesizer {
    state: RefCell<State>,
    // Settings
    sample_rate: u32,
    normalizer: f32,
    pressed_fade_map: AmplitudeMap,
    released_fade_map: AmplitudeMap,
    // Message queue
    messages: Receiver<Message>,
    // Buffer for output.
    buffer: [f32; crate::audio::DEFAULT_RATE as usize],
}

impl Synthesizer {
    // -- Public methods ------------------------------------------------------
    pub fn new(notes_number: usize, sample_rate: u32) -> (Synthesizer, SynthesizerHandle) {
        let (tx, rx) = crossbeam_channel::bounded(CHANNEL_SIZE);
        let obertones = vec![(1.0, 1.0), (2.0, 0.25), (4.0, 0.125)];
        let normalizer = utils::get_obertone_normalizer(&obertones);

        let notes = (0..notes_number)
            .map(|i| Tone::new(utils::get_note_frequency(i)))
            .collect();

        let state = State {
            notes,
            position: 0,
            obertones,
            active_notes: Vec::with_capacity(notes_number),
        };
        let synthesizer = Synthesizer {
            state: RefCell::new(state),
            sample_rate,
            normalizer,
            pressed_fade_map: amplitude::default_hold(),
            released_fade_map: amplitude::default_release(),
            messages: rx,
            buffer: [0.0; crate::audio::DEFAULT_RATE as usize],
        };

        (synthesizer, tx)
    }

    fn note_hold(&mut self, note_id: usize) {
        self.state.get_mut().note_hold(note_id);
    }

    fn note_release(&mut self, note_id: usize) {
        self.state.get_mut().note_release(note_id);
    }
}

impl State {
    fn note_hold(&mut self, note_id: usize) {
        self.notes.get_mut(note_id).map(Tone::hold);
    }

    fn note_release(&mut self, note_id: usize) {
        self.notes.get_mut(note_id).map(Tone::release);
    }

    fn advance_sample_state(
        &mut self,
        sample_rate: u32,
        pressed_fade_map: &AmplitudeMap,
        released_fade_map: &AmplitudeMap,
    ) {
        self.position = (self.position + 1) % (sample_rate * 60);
        for note in self.notes.iter_mut().filter(|note| note.is_active) {
            let time = note.hold_elapsed_cycles as f32 / sample_rate as f32;
            note.amplitude_target = pressed_fade_map.get_value(time);
            if !note.is_held {
                let time = note.release_elapsed_cycles as f32 / sample_rate as f32;
                note.amplitude_target *= released_fade_map.get_value(time)
            }
            note.advance();
        }
    }
}

// -- Impl AudioSource trait --------------------------------------------------

impl AudioSource for Synthesizer {
    fn get_samples(&mut self, n_frames: usize) -> [&[f32]; 2] {
        let normalizer = self.normalizer;
        let sample_rate = self.sample_rate;
        let pressed_fade_map = &self.pressed_fade_map;
        let released_fade_map = &self.released_fade_map;
        let buffer = &mut self.buffer[0..n_frames];
        let state = self.state.get_mut();

        // active notes haven't change, so we get them once.
        state.active_notes.truncate(0);
        state.active_notes.extend(
            state
                .notes
                .iter()
                .enumerate()
                .filter(|(_, note)| note.is_active)
                .map(|(idx, _)| idx),
        );

        buffer.iter_mut().for_each(|value| {
            state.advance_sample_state(sample_rate, pressed_fade_map, released_fade_map);
            let time = state.position as f32 / sample_rate as f32;
            *value = state
                .active_notes
                .iter()
                .map(|idx| {
                    let note = &state.notes[*idx];
                    let x = time * note.frequency * std::f32::consts::TAU;
                    state
                        .obertones
                        .iter()
                        .map(|(mul, den)| (mul * x).sin() * den)
                        .sum::<f32>()
                        * note.amplitude_current
                        / normalizer
                })
                .sum::<f32>()
        });
        [&self.buffer, &self.buffer]
    }

    fn process_messages(&mut self) {
        loop {
            match self.messages.try_recv() {
                Ok(Message::NoteHold(id)) => self.note_hold(id),
                Ok(Message::NoteRelease(id)) => self.note_release(id),
                Err(_) => break,
            }
        }
    }
}
