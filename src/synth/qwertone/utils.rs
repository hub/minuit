// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) Andrii Zymohliad
// Originally copied fro https://gitlab.com/azymohliad/qwertone/-/blob/master/src/audio/sources/synthesizer/utils.rs
//

const A4_FREQUENCY: f32 = 440.0;
const A4_ID: usize = 33;

pub fn get_note_frequency(note_id: usize) -> f32 {
    A4_FREQUENCY * 2f32.powf((note_id as f32 - A4_ID as f32) / 12.0)
}

pub fn get_obertone_normalizer(obertones: &[(f32, f32)]) -> f32 {
    obertones.iter().map(|ob| ob.1).sum::<f32>() + 1f32
}
