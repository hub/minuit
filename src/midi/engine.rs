// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2020-2023 Hubert Figuière
//

//! `midi::engine` encompass much of the MIDI code.
//!

use std::cell::{Cell, RefCell};

use midi_control as midi;
use midi_control::{Channel, MidiMessage};

use crate::synth;

/// Message for MIDI control
pub enum MidiControlMessage {
    /// Dispatch the message directly from the MIDI stack
    Dispatch(MidiMessage),
    /// Set the synth
    SetSynth(Option<synth::BoxedHandle>),
    /// Filter channel
    FilterChannel(midi::Channel),
    /// A soundbank is now available (requested and downloaded)
    SoundbankAvailable(String, std::path::PathBuf),
}

pub type MidiControlSender = std::sync::mpsc::Sender<MidiControlMessage>;

/// The MIDI Control will receive MIDI messages from the controller and pass them.
///
/// It will also receive messages from the app, like synthesized MIDI
/// messages or "soundbank available".
///
pub struct MidiControl {
    /// The MIDI channel it listen to.
    channel: Cell<Channel>,
    /// The currently playing synthesizer to control.
    synthesizer: RefCell<Option<synth::BoxedHandle>>,
    /// Callback for MIDI messages received back to the UI.
    callback: Box<dyn Fn(MidiMessage) + Send>,
}

impl MidiControl {
    /// Create a new `MidiControl` and returns the channel sender.
    pub fn sender<F>(callback: F) -> MidiControlSender
    where
        F: Fn(MidiMessage) + Send + 'static,
    {
        let (sender, receiver) = std::sync::mpsc::channel::<MidiControlMessage>();

        let me = MidiControl {
            channel: Cell::new(Channel::Invalid),
            synthesizer: RefCell::new(None),
            callback: Box::new(callback),
        };

        print_on_err!(std::thread::Builder::new()
            .name("midi control".to_string())
            .spawn(move || {
                while let Ok(c) = receiver.recv() {
                    match c {
                        MidiControlMessage::Dispatch(midi) => me.dispatch(midi),
                        MidiControlMessage::SoundbankAvailable(ref s, ref p) => {
                            me.soundbank_available(s, p)
                        }
                        MidiControlMessage::SetSynth(synth) => me.set_synth(synth),
                        MidiControlMessage::FilterChannel(ch) => me.set_channel_filter(ch),
                    }
                }
            }));

        sender
    }

    /// Handle a `soundbank` being made available at `p`.
    fn soundbank_available(&self, soundbank: &str, p: &std::path::Path) {
        if let Some(s) = self.synthesizer.borrow().as_ref() {
            s.soundbank_available(soundbank, p);
        }
    }

    /// Set the MIDI `channel` to filter.
    fn set_channel_filter(&self, channel: Channel) {
        self.channel.set(channel);
    }

    /// Set the synth that will play.
    fn set_synth(&self, synth: Option<synth::BoxedHandle>) {
        self.synthesizer.replace(synth);
    }

    /// Dispatch a MIDI message to the various channel senders.
    ///
    /// `command`: a [`MidiMessage`].
    ///
    /// Will pass the MIDI message to the synth handle if `self.channel` matches,
    /// and then send it to the `self.sender` channel.
    /// This is called by the MIDI driver.
    fn dispatch(&self, command: MidiMessage) {
        if let Some(ref synth) = *self.synthesizer.borrow() {
            let my_channel = self.channel.get();
            let ch = command.get_channel();
            // Channel::Invalid is treated as Omni.
            if my_channel == Channel::Invalid || ch == Channel::Invalid || ch == my_channel {
                match command {
                    MidiMessage::NoteOn(_, ref e) => synth.note_on(e),
                    MidiMessage::NoteOff(_, ref e) => {
                        synth.note_off(e);
                    }
                    MidiMessage::PitchBend(_, lsb, msb) => {
                        let mut val: u16;
                        val = lsb.into();
                        val += (msb as u16) << 7;
                        synth.pitch_bend(val);
                    }
                    _ => {}
                }
            }
        }
        (self.callback)(command);
    }
}
