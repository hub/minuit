// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2022-2023 Hubert Figuière
//

pub mod engine;

use std::cell::RefCell;
use std::rc::Rc;

use midi_control as midi;

use engine::{MidiControlMessage, MidiControlSender};

/// Store the MIDI state: the [input][midir::MidiInput].
///
/// This is currently highly peculiar to [`midir`].
pub(crate) struct MidiState {
    /// The MIDI input.
    pub input: Option<midir::MidiInput>,
    /// The MIDI connection. This will hold a ref to `midi_sender`.
    pub connection: Option<midir::MidiInputConnection<MidiControlSender>>,
    /// The [MIDI control][MidiControlSender].
    pub midi_sender: MidiControlSender,
}

impl MidiState {
    pub fn new(midi_sender: MidiControlSender) -> Rc<RefCell<MidiState>> {
        Rc::new(RefCell::new(MidiState {
            input: midir::MidiInput::new("Compiano").ok(),
            connection: None,
            midi_sender,
        }))
    }

    /// Get a displayable version of the `port_name`.
    ///
    /// Currently just split the string and return the first element
    /// or the whole.
    pub fn displayable_port_name(port_name: &str) -> &str {
        port_name.split(':').next().unwrap_or(port_name)
    }

    /// List the port names even if the connection is open.
    pub fn list_port_names() -> Vec<String> {
        if let Ok(input) = midir::MidiInput::new("Compiano-list-ports") {
            input
                .ports()
                .iter()
                .map(|port| input.port_name(port).ok().unwrap_or_default())
                .collect()
        } else {
            vec![]
        }
    }

    /// Get the port name at index.
    ///
    /// Will return `None` if a connection is open.
    pub fn port_name_at(&self, idx: usize) -> Option<String> {
        self.input.as_ref().and_then(|midi_input| {
            let port = &midi_input.ports()[idx];
            midi_input.port_name(port).ok()
        })
    }

    /// Get the port index with name.
    ///
    /// Will return `None` if a connection is open.
    pub fn port_index_for_name(&self, name: &str) -> Option<usize> {
        let port_count = self.input.as_ref().map(|input| input.port_count())?;
        (0..port_count).find(|idx| {
            self.port_name_at(*idx)
                .map(|port_name| port_name == name)
                .unwrap_or(false)
        })
    }

    /// Connect to the MIDI port with name `port_name`.
    pub fn connect(&mut self, port_name: &str) -> Result<(), midir::ConnectErrorKind> {
        let idx = self
            .port_index_for_name(port_name)
            .ok_or(midir::ConnectErrorKind::InvalidPort)?;
        let input = self.input.take().unwrap();
        let port = &input.ports()[idx];
        let connection = input.connect(
            port,
            port_name,
            move |_timestamp, data, engine| {
                let command = midi::MidiMessage::from(data);
                // println!("{}: received {:?} => {:?}", timestamp, data, command);
                print_on_err!(engine.send(MidiControlMessage::Dispatch(command)));
            },
            self.midi_sender.clone(),
        );
        match connection {
            Err(err) => {
                let kind = err.kind();
                self.input = Some(err.into_inner());
                Err(kind)
            }
            connection => {
                self.connection = connection.ok();
                Ok(())
            }
        }
    }

    /// Close the connection before reopening it.
    pub fn close(&mut self) {
        // Recover the midi input by closing the connection.
        if self.connection.is_some() {
            let connection = self.connection.take().unwrap();
            let (input, _) = connection.close();
            self.input = Some(input);
        }
    }
}

#[cfg(test)]
mod test {
    use super::MidiState;

    #[test]
    fn test_displayable_port_name() {
        assert_eq!(
            MidiState::displayable_port_name(
                "Arturia MiniLab mkII:Arturia MiniLab mkII MIDI 1 20:0"
            ),
            "Arturia MiniLab mkII",
        );
        assert_eq!(MidiState::displayable_port_name("foobar"), "foobar");
    }
}
