// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2020 Hubert Figuière
//

/// Print a message on error returned.
macro_rules! print_on_err {
    ($e:expr) => {
        if let Err(err) = $e {
            log::error!(
                "{}:{} Error '{}': {:?}",
                file!(),
                line!(),
                stringify!($e),
                err
            );
        }
    };
}

#[cfg(test)]
pub fn init_test_logger() {
    let _ = env_logger::builder().is_test(true).try_init();
}
