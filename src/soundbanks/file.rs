// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2022-2024 Hubert Figuière
//

use std::io::Read;

use async_trait::async_trait;
use log::debug;
use sha2::Digest;

use super::downloader::{Downloader, DownloaderResult, Error};
use super::Soundbank;

/// Downloader for a single file
pub struct SingleFileDownloader {
    /// The
    dest_root: std::path::PathBuf,
    /// The bank id.
    bank_id: String,
    url: Option<String>,
    sha256: Option<String>,
    /// delegate to manage accepted file types and post processing.
    delegate: Box<dyn FileDownloaderDelegate>,
}

impl SingleFileDownloader {
    /// Create a new single file downloader.
    ///
    /// `soudbanks_path` is the root of the soundbanks download.
    ///
    /// `soundbank`: the soundbank to download.
    pub fn new(soundbanks_path: &std::path::Path, soundbank: &Soundbank) -> SingleFileDownloader {
        Self::with_delegate(
            soundbanks_path,
            soundbank,
            Box::new(SingleFileDownloaderDelegate {}),
        )
    }

    /// Create a file downloader with a delegate to perform extra tasks like unarchiving.
    ///
    /// `soudbanks_path` is the root of the soundbanks download.
    ///
    /// `soundbank`: the soundbank to download.
    ///
    /// `delegate`: the delegate implementing [`FileDownloaderDelegate`] to perform the extra
    /// tasks upond download.
    pub fn with_delegate(
        soundbanks_path: &std::path::Path,
        soundbank: &Soundbank,
        delegate: Box<dyn FileDownloaderDelegate>,
    ) -> SingleFileDownloader {
        SingleFileDownloader {
            dest_root: soundbanks_path.to_path_buf(),
            bank_id: soundbank.id.clone(),
            url: soundbank.url.clone(),
            sha256: soundbank.sha256.clone(),
            delegate,
        }
    }
}

/// Trait for the delegate implementation.
pub trait FileDownloaderDelegate: Send + Sync {
    /// Returns if the `content_type` is acceptable.
    ///
    /// `content_type` is a MIME value as str.
    fn is_content_type_acceptable(&self, content_type: &str) -> bool;
    /// Finalize the download
    ///
    /// `fname` is the full path to the downloaded file in a temporary location.
    ///
    /// `dest_dir` is the destination directory where the finalized download is put.
    /// Like the unarchived content, or just simply the file.
    fn finalize_download(
        &self,
        content_type: &str,
        fname: &std::path::Path,
        dest_dir: &std::path::Path,
    ) -> DownloaderResult;
}

/// Delegate to download single files.
struct SingleFileDownloaderDelegate {}

impl FileDownloaderDelegate for SingleFileDownloaderDelegate {
    fn is_content_type_acceptable(&self, _: &str) -> bool {
        true
    }

    /// For single file download, this will copy the file to its final location.
    fn finalize_download(
        &self,
        _content_type: &str,
        fname: &std::path::Path,
        dest_dir: &std::path::Path,
    ) -> DownloaderResult {
        // copy file to dest_dir
        if let Some(dest_name) = fname.file_name() {
            std::fs::create_dir_all(dest_dir)?;
            let mut dest = std::path::PathBuf::from(dest_dir);
            dest.push(dest_name);
            debug!("Copying {:?} to {:?}", fname, dest);
            std::fs::copy(fname, dest)?;
            Ok(dest_dir.to_path_buf())
        } else {
            Err(Error::InvalidParameters)
        }
    }
}

#[async_trait]
impl Downloader for SingleFileDownloader {
    fn get_target_directory(&self) -> std::path::PathBuf {
        self.dest_root.clone().join(&self.bank_id)
    }

    async fn download(&self) -> DownloaderResult {
        if self.url.is_none() || self.sha256.is_none() {
            return Err(Error::InvalidParameters);
        }
        let url = self.url.as_ref().unwrap();
        let tmp_dir = tempfile::tempdir().expect("couldn't create temp dir");
        let fname = {
            let url = url::Url::parse(url).unwrap();
            let fname = url
                .path_segments()
                .and_then(|segments| segments.last())
                .and_then(|name| if name.is_empty() { None } else { Some(name) })
                .unwrap();
            tmp_dir.path().join(fname)
        };
        let sha256 = self.sha256.as_ref().unwrap();

        debug!("creating request");
        let response = ureq::get(url).call().map_err(Box::new)?;

        let mut dest = std::fs::File::create(&fname)?;

        debug!("getting headers from response");
        let content_type = response.content_type().to_string();
        self.delegate.is_content_type_acceptable(&content_type);

        debug!("got type {:?}", content_type);
        let byte_count = response
            .header("Content-Length")
            .and_then(|v| v.parse().ok())
            .unwrap_or(0_usize);
        debug!("got {} bytes", byte_count);

        // Download and checksum
        // It is done in chunks of 1MB.
        let mut reader = response.into_reader();
        let mut hasher = sha2::Sha256::new();
        let mut buffer = vec![0u8; 1_000_000];
        let mut downloaded_count = 0_usize;
        loop {
            match reader.read(buffer.as_mut_slice()) {
                Ok(count) => {
                    downloaded_count += count;
                    let mut buf = &buffer[..count];
                    hasher.update(buf);
                    std::io::copy(&mut buf, &mut dest)?;
                    if byte_count > 0 && downloaded_count >= byte_count {
                        break;
                    }
                }
                Err(e) => {
                    if e.kind() == std::io::ErrorKind::BrokenPipe {
                        log::error!("Broken pipe");
                        break;
                    }
                    return Err(e.into());
                }
            }
        }
        let digest = hasher.finalize();
        let sha256_digest = format!("{digest:x}");
        debug!("checksum is {}", &sha256_digest);
        debug!("Copied content to {:?}", &fname);

        if &sha256_digest != sha256 {
            log::error!(
                "Wrong checksum. Expected {}, got {}",
                sha256,
                &sha256_digest
            );
            return Err(Error::Checksum);
        }

        let dest_dir =
            self.delegate
                .finalize_download(&content_type, &fname, &self.get_target_directory())?;

        debug!("Download complete: {:?}", &dest_dir);
        Ok(dest_dir)
    }
}

#[cfg(test)]
mod tests {
    use super::super::downloader::Downloader;
    use super::super::{Soundbank, Type};
    use super::SingleFileDownloader;

    #[async_std::test]
    async fn test_file_download() -> std::io::Result<()> {
        crate::utils::init_test_logger();
        // create soundbank
        let soundbank = Soundbank {
            id: "testfile".to_string(),
            name: "test file soundbank".to_string(),
            path: None,
            r#type: Type::File,
            url: Some(
                "https://github.com/hfiguiere/soundbank_test/archive/refs/tags/0.1.0.zip"
                    .to_string(),
            ),
            sha256: Some(
                "2d27c5f05b3a021dcb73aa6d9d86cf9356f410a094f5d9f0103a77a9b4e4b6d7".to_string(),
            ),
            commit: None,
        };
        // create the soundbanks location (directory)
        let temp_dir = tempfile::tempdir().expect("can't create tempdir");
        let soundbank_path = temp_dir.path().join("soundbanks");

        let downloader = SingleFileDownloader::new(&soundbank_path, &soundbank);
        let result = downloader.download().await;

        assert!(result.is_ok());
        let content_path = result.unwrap();
        assert_eq!(content_path.parent().unwrap(), soundbank_path);

        Ok(())
    }
}
