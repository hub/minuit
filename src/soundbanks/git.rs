// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2021-2024 Hubert Figuière
//

use async_trait::async_trait;
use git2::Repository;

use super::downloader::{Downloader, Error};
use super::Soundbank;

/// A downloader task for git storage.
pub struct GitDownloader {
    /// The soundbank root. The result will be a sub directory.
    dest_root: std::path::PathBuf,
    /// Bank ID
    bank_id: String,
    /// Git URL. None cause an error
    url: Option<String>,
    /// commit-ish: sha1, tag or branch. None cause an error.
    commit: Option<String>,
}

impl GitDownloader {
    /// Create a Git downloader.
    ///
    /// `soundbanks_path` is the root of the soundbanks.
    ///
    /// `soundbank` the soundbank to download.
    pub fn new(soundbanks_path: &std::path::Path, soundbank: &Soundbank) -> GitDownloader {
        GitDownloader {
            dest_root: soundbanks_path.to_path_buf(),
            bank_id: soundbank.id.clone(),
            url: soundbank.url.clone(),
            commit: soundbank.commit.clone(),
        }
    }
}

#[async_trait]
impl Downloader for GitDownloader {
    fn get_target_directory(&self) -> std::path::PathBuf {
        self.dest_root.clone().join(&self.bank_id)
    }

    async fn download(&self) -> super::DownloaderResult {
        if self.url.is_none() || self.commit.is_none() {
            return Err(Error::InvalidParameters);
        }

        let url = self.url.as_ref().unwrap();
        let commit = self.commit.as_ref().unwrap();

        let repo_dir = self.get_target_directory();

        Repository::clone(url, &repo_dir).and_then(|repo| {
            repo.revparse_single(commit)
                .map(|rev| rev.id())
                .and_then(|oid| {
                    repo.set_head_detached(oid)?;
                    repo.checkout_head(None)
                })
        })?;

        Ok(repo_dir)
    }
}

#[cfg(test)]
mod tests {
    use super::super::downloader::Downloader;
    use super::super::{Soundbank, Type};
    use super::GitDownloader;

    #[async_std::test]
    async fn test_git_download() -> std::io::Result<()> {
        crate::utils::init_test_logger();

        // create soundbank
        let soundbank = Soundbank {
            id: "test".to_string(),
            name: "test soundbank".to_string(),
            path: None,
            r#type: Type::Git,
            url: Some("https://github.com/hfiguiere/soundbank_test.git".to_string()),
            sha256: None,
            commit: Some("0.1.0".to_string()),
        };
        // create the soundbanks location (directory)
        let temp_dir = tempfile::tempdir().expect("can't create tempdir");
        let soundbank_path = temp_dir.path().join("soundbanks");

        let downloader = GitDownloader::new(&soundbank_path, &soundbank);
        let result = downloader.download().await;

        assert!(result.is_ok());
        let git_repo = result.unwrap();
        assert_eq!(git_repo.parent().unwrap(), soundbank_path);

        Ok(())
    }
}
