// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2021-2024 Hubert Figuière
//

use async_trait::async_trait;
use gettextrs::gettext as i18n;
use gtk4::glib;
use i18n_format::i18n_fmt;
use log::debug;

use super::archive::ArchiveDownloader;
use super::file::SingleFileDownloader;
use super::git::GitDownloader;
use super::Soundbank;
use crate::config;
use crate::toolkit;

/// Downloader error
#[derive(Debug, thiserror::Error)]
pub enum Error {
    /// Was already downloaded. It's all good
    #[error("Was already downloaded.")]
    AlreadyDownloaded,
    /// Checksum doesn't match
    #[error("Checksum doesn't match")]
    Checksum,
    /// Git error
    #[error("Git error: {0}")]
    Git(#[from] git2::Error),
    /// Not found
    #[error("Not found")]
    _NotFound,
    /// Invalid parameters
    #[error("Invalid parameters")]
    InvalidParameters,
    /// Invalid archive
    #[error("Invalid archive.")]
    InvalidArchive,
    /// IO Error wrapped.
    #[error("I/O error: {0}")]
    Io(#[from] std::io::Error),
    /// Lzma decompress error.
    #[error("LZMA error: {0}")]
    Lzma(#[from] lzma_rs::error::Error),
    /// Zip decompress error.
    #[error("Zip error: {0}")]
    Zip(#[from] zip::result::ZipError),
    #[error("Download error: {0}")]
    Ureq(#[from] Box<ureq::Error>),
}

/// Downloader result type.
pub type DownloaderResult = Result<std::path::PathBuf, Error>;

#[async_trait]
/// Trait for downloaders.
pub trait Downloader: Send {
    /// Perform download
    async fn download(&self) -> DownloaderResult;

    /// Get the target directory
    fn get_target_directory(&self) -> std::path::PathBuf;
}

/// Download manager, that will send back [messages][crate::AudioMessage].
pub struct DownloadManager {
    sender: async_channel::Sender<crate::AudioMessage>,
}

impl DownloadManager {
    /// Create the download manager with `sender` from the
    /// [`AudioStack`][crate::audio_stack::AudioStack].
    pub fn new(sender: async_channel::Sender<crate::AudioMessage>) -> Self {
        DownloadManager { sender }
    }

    /// Get the soundbank dir
    pub fn get_soundbank_dir() -> std::path::PathBuf {
        let mut data_dir = glib::user_data_dir();
        data_dir.push(config::APP_ID);
        data_dir.push("soundbanks");
        data_dir
    }

    /// Check if the download at `path` is valid.
    fn download_is_valid(path: &std::path::Path) -> bool {
        // XXX check the download is valid
        path.is_dir()
    }

    /// Initiate the download if needed
    ///
    /// `soundbank`: the [`Soundbank`] to download.
    ///
    /// Returns an error on failure.
    pub fn download(&self, soundbank: &Soundbank) -> Result<(), Error> {
        use super::Type;
        debug!("downloading {} to {:?}", &soundbank.name, &soundbank.path);
        let soundbank_id = soundbank.id.clone();

        if let Some(p) = &soundbank.path {
            if Self::download_is_valid(p) {
                let sender = self.sender.clone();
                let path = p.to_path_buf();
                toolkit::utils::send_async_any!(
                    crate::AudioMessage::SoundbankAvailable(soundbank_id, path),
                    sender
                );
                debug!("Already downloaded");
                return Err(Error::AlreadyDownloaded);
            }
        }

        debug!("need download");

        let soundbank_dir = Self::get_soundbank_dir();
        std::fs::create_dir_all(&soundbank_dir)?;
        let (tx, rx) = crossbeam_channel::bounded(5);
        let downloader: Box<dyn Downloader + Sync> = match soundbank.r#type {
            Type::Git => Box::new(GitDownloader::new(&soundbank_dir, soundbank)),
            Type::Archive => Box::new(ArchiveDownloader::new(&soundbank_dir, soundbank)),
            Type::File => Box::new(SingleFileDownloader::new(&soundbank_dir, soundbank)),
        };

        let p = downloader.get_target_directory();
        let sender = self.sender.clone();
        if Self::download_is_valid(&p) {
            debug!("Found target directory");
            toolkit::utils::send_async_any!(
                crate::AudioMessage::SoundbankAvailable(soundbank_id, p),
                sender
            );
            debug!("Already downloaded");
            return Err(Error::AlreadyDownloaded);
        } else {
            crate::ui::display_message_async(i18n_fmt! {
                // Translators: "{}" will be replaced by the soundbank name. Do not remove.
                i18n_fmt("Downloading soundbank {}.", &soundbank.name)
            });
            debug!("Spawning downloader, {:?}", &soundbank_dir);

            let d = downloader.download();
            if let Ok(path) = async_std::task::block_on(d) {
                print_on_err!(tx.send(path));
            }

            let path = rx.recv().unwrap();
            crate::ui::display_message_async(i18n("Download finished."));
            toolkit::utils::send_async_any!(
                crate::AudioMessage::SoundbankAvailable(soundbank_id, path),
                sender
            );
        }
        Ok(())
    }
}
