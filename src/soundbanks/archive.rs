// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2021-2024 Hubert Figuière
//

use async_trait::async_trait;
use log::debug;

use super::downloader::{Downloader, DownloaderResult, Error};
use super::file::{FileDownloaderDelegate, SingleFileDownloader};
use super::Soundbank;

#[derive(Debug)]
/// Type of archive
enum ArchiveType {
    Unknown,
    /// ZIP file.
    Zip,
    /// xz file.
    Xz,
}

impl From<&str> for ArchiveType {
    fn from(s: &str) -> ArchiveType {
        match s {
            "application/zip" => ArchiveType::Zip,
            "application/x-xz" => ArchiveType::Xz,
            _ => ArchiveType::Unknown,
        }
    }
}

/// Download an archive and extract it.
pub struct ArchiveDownloader {
    /// The concrete file downloader used to delegate the download.
    file_downloader: SingleFileDownloader,
}

impl ArchiveDownloader {
    /// Create an `ArchiveDownloader` that will download the file for the `soundbank`
    /// in a subfolder of `soundbanks_path`.
    ///
    /// `soundbank_path` is the root folder of the soundbanks.
    pub fn new(soundbanks_path: &std::path::Path, soundbank: &Soundbank) -> ArchiveDownloader {
        ArchiveDownloader {
            file_downloader: SingleFileDownloader::with_delegate(
                soundbanks_path,
                soundbank,
                Box::new(ArchiveDownloaderDelegate {}),
            ),
        }
    }
}

#[async_trait]
impl Downloader for ArchiveDownloader {
    fn get_target_directory(&self) -> std::path::PathBuf {
        self.file_downloader.get_target_directory()
    }

    async fn download(&self) -> DownloaderResult {
        self.file_downloader.download().await
    }
}

/// Delegate for the Archive Downloader to accept archives and decompress them
struct ArchiveDownloaderDelegate {}

impl FileDownloaderDelegate for ArchiveDownloaderDelegate {
    fn is_content_type_acceptable(&self, header: &str) -> bool {
        match ArchiveType::from(header) {
            ArchiveType::Unknown => {
                debug!("Unknown type {:?}", header);
                false
            }
            _ => true,
        }
    }

    fn finalize_download(
        &self,
        content_type: &str,
        fname: &std::path::Path,
        dest_dir: &std::path::Path,
    ) -> DownloaderResult {
        let archive_type = ArchiveType::from(content_type);
        match archive_type {
            ArchiveType::Zip => {
                let dest = std::fs::File::open(fname)?;
                debug!("Unziping archive into {:?}", &dest_dir);
                zip::ZipArchive::new(dest).and_then(|mut archive| archive.extract(dest_dir))?;
                Ok(dest_dir.to_path_buf())
            }
            ArchiveType::Xz => {
                let mut archive = std::io::BufReader::new(std::fs::File::open(fname)?);
                let post_archive = fname.with_extension("");
                if post_archive.extension() == Some(&std::ffi::OsString::from("tar")) {
                    {
                        let mut decomp = std::fs::File::create(&post_archive)?;
                        debug!("unxz tar file into {:?}", &post_archive);
                        lzma_rs::xz_decompress(&mut archive, &mut decomp)?;
                    }
                    //
                    let mut ar = tar::Archive::new(std::fs::File::open(post_archive)?);
                    debug!("untar archive into {:?}", dest_dir);
                    ar.unpack(dest_dir)?;
                } else {
                    let mut dest_file = dest_dir.to_path_buf();
                    dest_file.push(post_archive.file_name().unwrap());
                    let mut decomp = std::fs::File::create(&dest_file)?;
                    debug!("unxz file into {:?}", &dest_file);
                    lzma_rs::xz_decompress(&mut archive, &mut decomp)?;
                }
                Ok(dest_dir.to_path_buf())
            }
            _ => Err(Error::InvalidArchive),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::super::downloader::Downloader;
    use super::super::{Soundbank, Type};
    use super::ArchiveDownloader;

    #[async_std::test]
    async fn test_zip_download() -> std::io::Result<()> {
        crate::utils::init_test_logger();
        // create soundbank
        let soundbank = Soundbank {
            id: "testzip".to_string(),
            name: "test zip soundbank".to_string(),
            path: None,
            r#type: Type::Archive,
            url: Some(
                "https://github.com/hfiguiere/soundbank_test/archive/refs/tags/0.1.0.zip"
                    .to_string(),
            ),
            sha256: Some(
                "2d27c5f05b3a021dcb73aa6d9d86cf9356f410a094f5d9f0103a77a9b4e4b6d7".to_string(),
            ),
            commit: None,
        };
        // create the soundbanks location (directory)
        let temp_dir = tempfile::tempdir().expect("can't create tempdir");
        let soundbank_path = temp_dir.path().join("soundbanks");

        let downloader = ArchiveDownloader::new(&soundbank_path, &soundbank);
        let result = downloader.download().await;

        let content_path = result.expect("Result to be ok");
        assert_eq!(content_path.parent().unwrap(), soundbank_path);

        Ok(())
    }
}
