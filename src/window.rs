// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2020-2024 Hubert Figuière
//

use std::cell::{Cell, RefCell};
use std::collections::HashMap;
use std::rc::Rc;
use std::sync::Arc;

use crate::audio_stack::AudioStack;
use crate::events;
use crate::instruments;
use crate::instruments::{Preset, FACTORY_PRESETS};
use crate::midi::engine::{MidiControl, MidiControlMessage};
use crate::midi::MidiState;
use crate::settings;
use crate::toolkit;
use crate::ui::{self, DialogController};
use crate::widgets::piano_widget::{PianoWidget, PianoWidgetExt};

use adw::prelude::*;
use gettextrs::gettext as i18n;
use gtk4::gio;
use gtk4::glib;
use gtk4::subclass::prelude::*;
use midi_control as midi;

/// Window controller.
///
/// Will own the `Window`, the [audio stack][crate::audio_stack::AudioStack] amd
/// a sender channel for the [UI event messages][events::Message].
pub struct WindowController {
    /// The [`Window`] widget.
    pub w: Window,
    /// The [`AudioStack`]: the main part of the audio playback.
    audio_stack: Arc<AudioStack>,
    midi_state: Rc<RefCell<MidiState>>,
    sender: async_channel::Sender<events::Message>,
    /// Last dialog id.
    last_id: Cell<usize>,
    /// The dialogs by id.
    dialogs: RefCell<HashMap<usize, Rc<dyn ui::DialogController>>>,
}

/// Set the channel value in the gtk4::Label.
fn set_channel_value(control: &gtk4::Label, ch: midi::Channel) {
    if ch == midi::Channel::Invalid {
        control.set_label(&i18n("any"));
    } else {
        control.set_label(&(ch as u8 + 1).to_string());
    }
}

impl WindowController {
    pub fn new(app: &adw::Application, sender: async_channel::Sender<events::Message>) -> Self {
        let win = Window::new(app);

        let piano = win.piano().expect("Failed to create piano");
        let sender2 = sender.clone();
        let midi_control = MidiControl::sender(move |msg| {
            let sender = sender2.clone();
            match msg {
                midi::MidiMessage::NoteOn(_, e) => {
                    toolkit::utils::send_async_any!(events::Message::PianoKeyDown(e.key), sender)
                }
                midi::MidiMessage::NoteOff(_, e) => {
                    toolkit::utils::send_async_any!(events::Message::PianoKeyUp(e.key), sender)
                }
                _ => {}
            }
        });
        let audio_stack = AudioStack::new(midi_control.clone());
        let midi_state = MidiState::new(midi_control.clone());

        piano.connect_note_pressed(glib::clone!(
            #[strong]
            midi_control,
            move |_, note| print_on_err!(midi_control.send(MidiControlMessage::Dispatch(
                midi::MidiMessage::NoteOn(
                    midi::Channel::Invalid,
                    midi::KeyEvent {
                        key: note,
                        value: 127,
                    },
                )
            )))
        ));
        piano.connect_note_released(glib::clone!(
            #[strong]
            midi_control,
            move |_, note| print_on_err!(midi_control.send(MidiControlMessage::Dispatch(
                midi::MidiMessage::NoteOff(
                    midi::Channel::Invalid,
                    midi::KeyEvent {
                        key: note,
                        value: 0,
                    },
                )
            )))
        ));

        let obj = Self {
            w: win.clone(),
            audio_stack,
            midi_state,
            sender,
            last_id: Cell::new(0),
            dialogs: RefCell::new(HashMap::new()),
        };

        obj.setup_actions();
        if let Some(instruments_combo) = win.instruments_combo() {
            obj.populate_instruments(&instruments_combo);
        }
        if let Some(midi_device) = win.midi_device() {
            if let Some(midi_channel) = win.midi_channel() {
                obj.populate_midi(&midi_device, &midi_channel);
            }
        }

        obj
    }

    /// Add a [`DialogController`].
    ///
    /// Return the id of the controller.
    fn add_controller(&self, dialog: Rc<dyn ui::DialogController>) -> usize {
        let mut id = self.last_id.get();
        id += 1;
        self.last_id.set(id);
        self.dialogs.borrow_mut().insert(id, dialog);

        id
    }

    /// Remove a [`DialogController`] with `id`.
    pub fn remove_controller(&self, id: usize) {
        self.dialogs.borrow_mut().remove(&id);
    }

    /// Get the [`Preset`] with `preset_id`.
    pub fn get_preset(preset_id: &str) -> Option<Preset> {
        FACTORY_PRESETS.lock().unwrap().get(preset_id).cloned()
    }

    fn setup_actions(&self) {
        let win = self.w.clone().upcast::<gtk4::ApplicationWindow>();
        let sender = self.sender.clone();
        action!(
            win,
            "shortcuts",
            glib::clone!(
                #[strong]
                sender,
                move |_, _| {
                    let sender = sender.clone();
                    toolkit::utils::send_async_local!(events::Message::KeyboardShortcuts, sender);
                }
            )
        );
        action!(
            win,
            "preferences",
            glib::clone!(
                #[strong]
                sender,
                move |_, _| {
                    let sender = sender.clone();
                    toolkit::utils::send_async_local!(events::Message::Preferences, sender);
                }
            )
        );
    }

    fn populate_instruments(&self, instruments: &gtk4::ComboBoxText) {
        // Sort the instruments by name, alphabetically
        let mut presets: Vec<(Option<String>, String)> = FACTORY_PRESETS
            .lock()
            .unwrap()
            .iter()
            .map(|(key, value)| (Some(key.into()), value.name.clone()))
            .collect();
        presets.sort_unstable_by(|a, b| a.1.partial_cmp(&b.1).unwrap());
        for preset in presets {
            instruments.append(preset.0.as_deref(), &preset.1);
        }

        let sender = self.sender.clone();
        let audio_stack = self.audio_stack.clone();
        instruments.connect_changed(glib::clone!(
            #[weak]
            audio_stack,
            move |w| {
                if let Some(preset) = w.active_id().and_then(|id| {
                    let sender = sender.clone();
                    let id_str = id.to_string();
                    toolkit::utils::send_async_local!(
                        events::Message::InstrumentSelected(id_str),
                        sender
                    );
                    Self::get_preset(id.as_str())
                }) {
                    audio_stack.set_synth(&preset, &sender);
                }
            }
        ));

        let selected = self.w.settings().string(settings::SELECTED_INSTRUMENT);
        instruments.set_active_id(Some(selected.as_str()));
    }

    /// Populate the MIDI UI and connect to the settings for update.
    fn populate_midi(&self, midi_device: &gtk4::Label, midi_channel: &gtk4::Label) {
        let settings = self.w.settings();

        settings.connect_changed(
            Some(settings::MIDI_INPUT_DEVICE),
            glib::clone!(
                #[strong]
                midi_device,
                move |settings, detail| {
                    let value = settings.string(detail);
                    midi_device.set_label(MidiState::displayable_port_name(&value));
                }
            ),
        );
        let value = settings.string(settings::MIDI_INPUT_DEVICE);
        self.connect_midi(&value);
        midi_device.set_label(MidiState::displayable_port_name(&value));

        settings.connect_changed(
            Some(settings::MIDI_INPUT_CHANNEL),
            glib::clone!(
                #[strong]
                midi_channel,
                move |settings, detail| {
                    let value = settings.uint(detail);
                    set_channel_value(&midi_channel, midi::Channel::from(value as u8));
                }
            ),
        );
        let value = settings.uint(settings::MIDI_INPUT_CHANNEL);
        let ch = midi::Channel::from(value as u8);
        self.set_midi_channel(ch);
        set_channel_value(midi_channel, ch);
    }

    pub fn midi_device_selected(&self, device: &str) {
        let settings = self.w.settings();
        print_on_err!(settings.set_string(settings::MIDI_INPUT_DEVICE, device));
        self.connect_midi(device);
    }

    /// Do connect to the MIDI port.
    fn connect_midi(&self, port_name: &str) {
        let connected = self.midi_state.borrow_mut().connect(port_name);
        match connected {
            Ok(_) => {}
            Err(err) => eprintln!("Connection to {port_name} failed: {err}"),
        }
    }

    pub fn channel_selected(&self, ch: midi::Channel) {
        let settings = self.w.settings();
        print_on_err!(settings.set_uint(settings::MIDI_INPUT_CHANNEL, ch as u8 as u32));
        self.set_midi_channel(ch);
    }

    fn set_midi_channel(&self, ch: midi::Channel) {
        print_on_err!(self
            .midi_state
            .borrow()
            .midi_sender
            .send(MidiControlMessage::FilterChannel(ch)));
    }

    /// The `instrument` was selected (from `events::Message::InstrumentSelected`)
    pub fn instrument_selected(&self, instrument: &str) {
        print_on_err!(self
            .w
            .settings()
            .set_string(settings::SELECTED_INSTRUMENT, instrument));
        let preset = Self::get_preset(instrument);
        self.set_instrument(preset.as_ref());
        self.reset_instrument_ui();
    }

    /// Set the instrument using [`Preset`].
    pub fn set_instrument(&self, preset: Option<&Preset>) {
        if let Some(ref w) = self.w.widgets() {
            w.instrument_info_popover.set_preset(preset);
            if let Some(preset) = preset {
                w.omni_bar_label.set_text(&preset.name);
            }
        }
    }

    pub fn reset_instrument_ui(&self) {
        if let Some(ref w) = self.w.widgets() {
            w.reset_instrument_ui();
        }
    }

    /// Show the instrument UI.
    ///
    /// The instrument UI is the one that the plugin can request.
    pub fn show_instrument_ui(
        &self,
        ui: &str,
        instrument_ui: Option<Box<dyn instruments::InstrumentUi>>,
    ) {
        if let Some(ref w) = self.w.widgets() {
            w.show_instrument_ui(ui, instrument_ui);
        }
    }

    pub fn show_preferences(&self) {
        let dialog: Rc<ui::preferences::PreferenceDialog> =
            ui::dialog_controller::new_dialog::<ui::preferences::PreferenceDialog>();
        let id = self.add_controller(dialog.clone());
        let sender = self.sender.clone();
        dialog.setup(sender.clone(), self.midi_state.clone(), self.w.settings());
        dialog.show(
            &(&self.w).into(),
            Box::new(move || {
                let sender = sender.clone();
                toolkit::utils::send_async_local!(events::Message::PreferencesClosed(id), sender);
            }),
        );
    }

    pub fn display_message(&self, msg: &str) {
        if let Some(ref w) = self.w.widgets() {
            w.display_message(msg);
        }
    }

    /// A piano key has been pressed.
    pub fn piano_key_down(&self, note: u8) {
        if let Some(ref w) = self.w.widgets() {
            w.piano.mark_midi(note, None);
        }
    }

    /// A piano key has been released.
    pub fn piano_key_up(&self, note: u8) {
        if let Some(ref w) = self.w.widgets() {
            w.piano.unmark_midi(note);
        }
    }
}

glib::wrapper! {
    /// The Compiano main window. Implemented as [`adw::ApplicationWindow`].
    pub struct Window(ObjectSubclass<imp::Window>)
        @extends gtk4::Widget, gtk4::Window, gtk4::ApplicationWindow, adw::ApplicationWindow;
}

impl From<&Window> for gtk4::Window {
    fn from(w: &Window) -> gtk4::Window {
        w.clone().upcast::<Self>()
    }
}

impl Window {
    pub fn new(app: &adw::Application) -> Self {
        glib::Object::with_mut_values(Self::static_type(), &mut [("application", app.into())])
            .downcast()
            .expect("Couldn't instanciate Window")
    }

    pub(self) fn widgets(&self) -> Option<Rc<imp::WindowWidgets>> {
        self.imp().widgets()
    }

    pub fn piano(&self) -> Option<PianoWidget> {
        self.imp().widgets().map(|w| w.piano.clone())
    }

    pub fn instruments_combo(&self) -> Option<gtk4::ComboBoxText> {
        self.imp().widgets().map(|w| w.instruments_combo.clone())
    }

    pub fn midi_device(&self) -> Option<gtk4::Label> {
        self.imp().widgets().map(|w| w.midi_device.clone())
    }

    pub fn midi_channel(&self) -> Option<gtk4::Label> {
        self.imp().widgets().map(|w| w.midi_channel.clone())
    }

    /// Get the [`Settings`][gio::Settings] instance for the window.
    pub fn settings(&self) -> &gio::Settings {
        self.imp().settings.get().expect("Could not get settings.")
    }

    fn save_window_size(&self) -> Result<(), glib::BoolError> {
        // Get the size of the window
        let size = self.default_size();

        // Set the window state in `settings`
        self.settings().set_int("main-window-width", size.0)?;
        self.settings().set_int("main-window-height", size.1)?;
        self.settings()
            .set_boolean("main-is-maximized", self.is_maximized())?;

        Ok(())
    }

    fn load_window_size(&self) {
        // Get the window state from `settings`
        let width = self.settings().int("main-window-width");
        let height = self.settings().int("main-window-height");
        let is_maximized = self.settings().boolean("main-is-maximized");

        // Set the size of the window
        self.set_default_size(width, height);

        // If the window was maximized when it was closed, maximize it again
        if is_maximized {
            self.maximize();
        }
    }
}

mod imp {

    use std::rc::Rc;

    use adw::prelude::*;
    use adw::subclass::prelude::*;
    use gettextrs::gettext as i18n;
    use gtk4::gio;
    use gtk4::glib;
    use libpanel::prelude::*;
    use once_cell::unsync::OnceCell;

    use crate::config;
    use crate::instruments;
    use crate::settings;
    use crate::widgets::piano_widget::PianoWidget;
    use crate::widgets::InstrumentInfoPopover;

    const INSTRUMENT_CHILD_NAME: &str = "instrument";
    const DEFAULT_CHILD_NAME: &str = "default";

    pub(super) struct WindowWidgets {
        /// The label from the omni bar.
        pub omni_bar_label: gtk4::Label,
        pub instrument_view: gtk4::Stack,
        pub instruments_combo: gtk4::ComboBoxText,
        pub instrument_info_popover: InstrumentInfoPopover,
        pub midi_device: gtk4::Label,
        pub midi_channel: gtk4::Label,
        pub piano: PianoWidget,
        toast_overlay: adw::ToastOverlay,
    }

    impl std::fmt::Debug for WindowWidgets {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            f.debug_struct("WindowWidgets")
                .field("instrument_view", &self.instrument_view)
                .field("instruments_combo", &self.instruments_combo)
                .field("midi_device", &self.midi_device)
                .field("midi_channel", &self.midi_channel)
                .field("piano", &self.piano)
                .finish()
        }
    }

    impl WindowWidgets {
        pub(super) fn reset_instrument_ui(&self) {
            if let Some(widget) = self.instrument_view.child_by_name(INSTRUMENT_CHILD_NAME) {
                self.instrument_view.remove(&widget);
                self.instrument_view
                    .set_visible_child_name(DEFAULT_CHILD_NAME);
            }
        }

        pub(super) fn show_instrument_ui(
            &self,
            ui: &str,
            instrument_ui: Option<Box<dyn instruments::InstrumentUi>>,
        ) {
            if let Some(mut instrument_ui) = instrument_ui {
                if let Some(instrument_widget) = instrument_ui.setup(ui) {
                    self.instrument_view
                        .add_named(&instrument_widget, Some(INSTRUMENT_CHILD_NAME));
                    self.instrument_view
                        .set_visible_child_name(INSTRUMENT_CHILD_NAME);
                }
            }
        }

        pub fn display_message(&self, msg: &str) {
            let toast = adw::Toast::new(msg);
            self.toast_overlay.add_toast(toast);
        }
    }

    #[derive(Default)]
    pub struct Window {
        widgets: OnceCell<Rc<WindowWidgets>>,
        /// `gio::Settings` instance used by app.
        pub(super) settings: OnceCell<gio::Settings>,
        //_receiver_connection: glib::SourceId,
    }

    impl ObjectImpl for Window {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();
            println!("profile = {}", config::PROFILE);
            if config::PROFILE == "Devel" {
                obj.add_css_class("devel");
            }
            // Required for gtk4::Builder to load it.
            let _ = PianoWidget::static_type();
            let builder = gtk4::Builder::from_resource("/net/figuiere/compiano/window.ui");

            // Main box
            get_widget!(builder, gtk4::Box, main_box);
            // populate the headerbar
            let instrument_info_popover = InstrumentInfoPopover::new();
            instrument_info_popover.set_position(gtk4::PositionType::Bottom);

            let omni_bar_label = self.setup_headerbar(&main_box, &instrument_info_popover);

            obj.set_content(Some(&main_box));

            get_widget!(builder, gtk4::Stack, instrument_view);
            get_widget!(builder, gtk4::ComboBoxText, instruments_combo);
            get_widget!(builder, gtk4::Label, midi_device);
            get_widget!(builder, gtk4::Label, midi_channel);
            get_widget!(builder, adw::ToastOverlay, toast_overlay);
            get_widget!(builder, PianoWidget, piano);

            let w = Rc::new(WindowWidgets {
                omni_bar_label,
                instrument_view,
                instruments_combo,
                instrument_info_popover,
                midi_device,
                midi_channel,
                piano,
                toast_overlay,
                //_receiver_connection: receiver_connection,
            });

            self.widgets.set(w).expect("Setting widgets");

            self.settings
                .set(settings::settings_source())
                .expect("Couldn't set `gio::Setting`");
            obj.load_window_size();
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Window {
        const NAME: &'static str = "CompianoWindow";
        type Type = super::Window;
        type ParentType = adw::ApplicationWindow;
    }

    impl Window {
        pub(super) fn widgets(&self) -> Option<Rc<WindowWidgets>> {
            self.widgets.get().cloned()
        }

        /// Setup the headerbar widget and returns it.
        fn setup_headerbar(
            &self,
            content: &gtk4::Box,
            instrument_info: &InstrumentInfoPopover,
        ) -> gtk4::Label {
            let header_bar = adw::HeaderBar::new();

            let omni_bar = libpanel::OmniBar::builder()
                .icon_name("audio-x-generic-symbolic")
                .popover(instrument_info)
                .build();
            let omni_bar_label = gtk4::Label::new(Some(&i18n("Now playing...")));
            omni_bar.add_prefix(0, &omni_bar_label);
            header_bar.set_title_widget(Some(&omni_bar));
            header_bar.set_show_start_title_buttons(true);
            header_bar.set_show_end_title_buttons(true);

            let button = gtk4::MenuButton::new();
            button.set_icon_name("open-menu-symbolic");
            let builder = gtk4::Builder::from_resource("/net/figuiere/compiano/window_menu.ui");
            get_widget!(builder, gio::Menu, main_menu);
            button.set_menu_model(Some(&main_menu));
            header_bar.pack_end(&button);
            content.prepend(&header_bar);

            omni_bar_label
        }
    }

    impl WidgetImpl for Window {}

    impl WindowImpl for Window {
        // Save window state right before the window will be closed
        fn close_request(&self) -> glib::Propagation {
            // Save window size
            self.obj()
                .save_window_size()
                .expect("Failed to save window state");

            // Don't inhibit the default handler
            glib::Propagation::Proceed
        }
    }

    impl ApplicationWindowImpl for Window {}
    impl AdwApplicationWindowImpl for Window {}
}
