// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2021-2023 Hubert Figuière
//

//! Toolkit module.

pub mod filemanager;

pub mod utils {
    use std::future::Future;

    use gtk4::glib;

    /// Send to an async channel from any thread.
    /// It's a macro because of the async block.
    macro_rules! send_async_any {
        ($message:expr, $sender:expr) => {
            $crate::toolkit::utils::spawn_any(async move {
                print_on_err!($sender.send($message).await);
            })
        };
    }

    /// Send to an async channel from the "local" thread.
    /// It's a macro because of the async block.
    macro_rules! send_async_local {
        ($message:expr, $sender:expr) => {
            $crate::toolkit::utils::spawn_local(async move {
                print_on_err!($sender.send($message).await);
            })
        };
    }

    pub(crate) use send_async_any;
    pub(crate) use send_async_local;

    // This comes from https://mmstick.github.io/gtkrs-tutorials/2x01-gtk-application.html

    pub fn spawn_local<F>(future: F)
    where
        F: Future<Output = ()> + 'static,
    {
        glib::MainContext::default().spawn_local(future);
    }

    pub fn spawn_any<F>(future: F)
    where
        F: Future<Output = ()> + Send + 'static,
    {
        glib::MainContext::default().spawn(future);
    }
}
