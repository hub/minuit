// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2022 Hubert Figuière
//

use dbus;
use dbus::blocking::Connection;
use gtk4::glib;

/// Create a startup ID from the XDG spec
fn startup_id() -> String {
    format!(
        "{}_{}_{}_{}",
        glib::prgname().unwrap_or_else(|| "compiano".into()),
        glib::host_name(),
        std::process::id(),
        glib::monotonic_time()
    )
}

/// Open folder at `uri` in the File Manager. Uses the DBus XDG API.
///
/// <https://www.freedesktop.org/wiki/Specifications/file-manager-interface/>
pub fn open_folder(uri: &str) -> Result<(), dbus::Error> {
    let startup_id = startup_id();
    let conn = Connection::new_session()?;
    let proxy = conn.with_proxy(
        "org.freedesktop.FileManager1",
        "/org/freedesktop/FileManager1",
        std::time::Duration::from_millis(5000),
    );

    proxy.method_call(
        "org.freedesktop.FileManager1",
        "ShowFolders",
        (vec![uri], startup_id),
    )
}
