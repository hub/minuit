// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2021-2025 Hubert Figuière
//

use std::cell::RefCell;
use std::path::{Path, PathBuf};

use adw::prelude::*;
use gtk4::gio;
use gtk4::glib;
use gtk4::glib::subclass::Signal;
use gtk4::subclass::prelude::*;

glib::wrapper! {
    /// A file chooser button to replace the one that was in Gtk3
    ///
    /// It will present itself as a button to select a file.
    ///
    /// Implemented as a [`gtk4::Button`].
    pub struct FileChooserButton(
        ObjectSubclass<FileChooserButtonPriv>)
        @extends gtk4::Button, gtk4::Widget;
}

impl FileChooserButton {
    /// Create a new `FileChooserButton`.
    pub fn new() -> FileChooserButton {
        glib::Object::new()
    }

    /// Get the path of the selected file, if any.
    pub fn get_filename(&self) -> Option<PathBuf> {
        self.imp().file.borrow().as_ref().and_then(|f| f.path())
    }

    /// Set the path of the selected file.
    pub fn set_filename<P: AsRef<Path>>(&self, f: P) {
        let file = gio::File::for_path(f.as_ref());
        self.set_property("file", &file);
    }
}

impl Default for FileChooserButton {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Default, glib::Properties)]
#[properties(wrapper_type = FileChooserButton)]
pub struct FileChooserButtonPriv {
    #[property(get, set, nick = "File", blurb = "The chosen file")]
    file: RefCell<Option<gio::File>>,
    dialog: RefCell<Option<gtk4::FileChooserNative>>,
    content: adw::ButtonContent,
}

#[glib::derived_properties]
impl ObjectImpl for FileChooserButtonPriv {
    fn constructed(&self) {
        self.parent_constructed();

        let obj = self.obj();
        self.content.set_icon_name("folder-documents-symbolic");
        obj.set_child(Some(&self.content));

        obj.bind_property("file", &self.content, "label")
            .transform_to(|_, file: gio::File| {
                file.basename()
                    .and_then(|name| Some(name.to_string_lossy().to_string()))
            })
            .build();

        obj.connect_clicked(move |b| {
            let file_chooser = {
                let mut builder = gtk4::FileChooserNative::builder()
                    .modal(true)
                    .action(gtk4::FileChooserAction::Open);
                if let Some(ref window) = b.root().and_then(|r| r.downcast::<gtk4::Window>().ok()) {
                    builder = builder.transient_for(window);
                }
                builder.build()
            };
            let priv_ = b.imp();
            // We must hold a reference to the Native dialog, or it crashes.
            priv_.dialog.replace(Some(file_chooser.clone()));
            let current_folder = priv_.file.borrow().as_ref().and_then(|f| f.parent());
            if current_folder.is_some() {
                print_on_err!(file_chooser.set_current_folder(current_folder.as_ref()));
            }
            file_chooser.connect_response(glib::clone!(
                #[weak]
                b,
                move |w, r| {
                    if r == gtk4::ResponseType::Accept {
                        b.set_property("file", w.file());
                        b.emit_by_name::<()>("file-set", &[]);
                    }
                    b.imp().dialog.replace(None);
                }
            ));
            file_chooser.show();
        });
    }

    fn signals() -> &'static [Signal] {
        use once_cell::sync::Lazy;
        static SIGNALS: Lazy<Vec<Signal>> =
            Lazy::new(|| vec![Signal::builder("file-set").run_last().build()]);
        SIGNALS.as_ref()
    }
}

impl WidgetImpl for FileChooserButtonPriv {}
impl ButtonImpl for FileChooserButtonPriv {}

#[glib::object_subclass]
impl ObjectSubclass for FileChooserButtonPriv {
    const NAME: &'static str = "FileChooserButton";
    type Type = FileChooserButton;
    type ParentType = gtk4::Button;
}
