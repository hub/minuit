// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2020-2022 Hubert Figuière
//

pub mod file_chooser_button;
pub mod instrument_info_popover;
pub mod piano_widget;

pub use instrument_info_popover::InstrumentInfoPopover;

use gtk4::prelude::*;

pub fn init() {
    file_chooser_button::FileChooserButton::static_type();
}
