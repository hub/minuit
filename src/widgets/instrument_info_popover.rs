// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2022 Hubert Figuière
//

use gtk4::glib;
use gtk4::subclass::prelude::*;

use crate::instruments::{ParamValue, Preset};

glib::wrapper! {
    /// Popover to display the instrument info.
    ///
    /// Implement as a template of [`gtk4::Popover`].
    pub struct InstrumentInfoPopover(ObjectSubclass<imp::InstrumentInfoPopover>)
        @extends gtk4::Popover, gtk4::Widget;
}

impl Default for InstrumentInfoPopover {
    fn default() -> Self {
        Self::new()
    }
}

impl InstrumentInfoPopover {
    pub fn new() -> Self {
        glib::Object::new()
    }

    /// Set the [`Preset`] whose info should be displayed.
    pub fn set_preset(&self, preset: Option<&Preset>) {
        let imp = self.imp();
        if let Some(preset) = preset {
            imp.name.set_label(&preset.name);
            imp.soundbank
                .set_label(preset.soundbank.as_deref().unwrap_or(""));
            imp.engine.set_label(&preset.synth);
            imp.author.set_label(
                preset
                    .params
                    .as_ref()
                    .and_then(|v| v.get("author"))
                    .and_then(ParamValue::to_str)
                    .unwrap_or(""),
            );
            imp.license.set_label(
                preset
                    .params
                    .as_ref()
                    .and_then(|v| v.get("license"))
                    .and_then(ParamValue::to_str)
                    .unwrap_or(""),
            );
        } else {
            imp.name.set_label("");
            imp.soundbank.set_label("");
            imp.engine.set_label("");
            imp.author.set_label("");
            imp.license.set_label("");
        }
    }
}

mod imp {
    use gtk4::glib;
    use gtk4::glib::subclass::prelude::*;
    use gtk4::glib::subclass::InitializingObject;
    use gtk4::subclass::prelude::*;

    #[derive(gtk4::CompositeTemplate, Default)]
    #[template(resource = "/net/figuiere/compiano/widgets/instrument_info_popover.ui")]
    pub struct InstrumentInfoPopover {
        #[template_child]
        pub(super) name: gtk4::TemplateChild<gtk4::Label>,
        #[template_child]
        pub(super) soundbank: gtk4::TemplateChild<gtk4::Label>,
        #[template_child]
        pub(super) engine: gtk4::TemplateChild<gtk4::Label>,
        #[template_child]
        pub(super) license: gtk4::TemplateChild<gtk4::Label>,
        #[template_child]
        pub(super) author: gtk4::TemplateChild<gtk4::Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for InstrumentInfoPopover {
        const NAME: &'static str = "InstrumentInfoPopover";
        type Type = super::InstrumentInfoPopover;
        type ParentType = gtk4::Popover;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for InstrumentInfoPopover {}

    impl WidgetImpl for InstrumentInfoPopover {}
    impl PopoverImpl for InstrumentInfoPopover {}
}
