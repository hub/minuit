// SPDX-License-Identifier: GPL-3.0-or-later
// SPDX-FileCopyrightText: (c) 2020-2025 Hubert Figuière
//

// Piano widget for Gtk
// Inspired by https://gitlab.gnome.org/GNOME/libgtkmusic/blob/master/src/PianoWidget.vala
//
// License GPLv3

use std::cell::{Cell, RefCell};
use std::collections::HashMap;
use std::convert::TryInto;

use gdk4::prelude::*;
use gtk4::cairo;
use gtk4::glib;
use gtk4::glib::subclass::Signal;
use gtk4::glib::translate::*;
use gtk4::prelude::*;
use gtk4::subclass::prelude::*;
use gtk4::subclass::widget::WidgetImplExt;

use midi_control::note::midi_is_accident;

glib::wrapper! {
    /// A widget to display a piano keyboard and interact with the keys.
    ///
    /// Currently implemented as a [`gtk4::DrawingArea`]
    pub struct PianoWidget(
        ObjectSubclass<PianoWidgetPriv>)
        @extends gtk4::DrawingArea, gtk4::Widget;
}

impl PianoWidget {
    pub fn new() -> PianoWidget {
        glib::Object::new()
    }
}

impl Default for PianoWidget {
    fn default() -> Self {
        Self::new()
    }
}

enum PianoError {
    InvalidCoordinates,
    OutOfRangeNote,
}

struct MarkedNoteStyle(gdk4::RGBA);

impl Default for MarkedNoteStyle {
    fn default() -> Self {
        MarkedNoteStyle(gdk4::RGBA::new(0.5, 0.5, 0.5, 1.0))
    }
}

/// Default key count: 25. Smallest MIDI controllers are 25 keys.
const DEFAULT_KEY_COUNT: u8 = 25;
/// Default first note is C4. C-Clef is C4, G-Clef is G4.
const DEFAULT_FIRST_NOTE: u8 = 48;
const MINIMUM_WIDTH: i32 = 240;
const MINIMUM_HEIGHT: i32 = 128;

#[derive(Default)]
struct Priv {
    h_padding: u16,
    v_padding: u16,
    accident_key_height: f64,
    accident_key_width: f64,
    nat_keys: u8,
    piano_width: f64,
    piano_height: f64,
    piano_x: f64,
    piano_y: f64,
    width: f64,
    height: f64,
    key_width: f64,
}

impl Priv {
    fn new() -> Self {
        Self {
            h_padding: 2,
            v_padding: 2,
            accident_key_height: 0.7,
            accident_key_width: 0.4,
            ..Self::default()
        }
    }
}

#[derive(glib::Properties)]
#[properties(wrapper_type = PianoWidget)]
pub struct PianoWidgetPriv {
    #[property(
        get,
        set,
        default_value = true,
        nick = "Auto-update",
        blurb = "Whether to auto-update or not"
    )]
    auto_update: Cell<bool>,
    #[property(get, set, minimum = 0, maximum = 127, default_value = DEFAULT_KEY_COUNT, nick = "Key count", blurb = "The number of keys on the keyboard")]
    key_count: Cell<u8>,
    #[property(get, set, minimum = 0, maximum = 127, default_value = DEFAULT_FIRST_NOTE, nick = "First note", blurb = "The first note on the keyboard")]
    first_note: Cell<u8>,
    nat_key_colour: RefCell<gdk4::RGBA>,
    accident_key_colour: RefCell<gdk4::RGBA>,
    #[property(
        get,
        set,
        default_value = false,
        nick = "Show Labels",
        blurb = "Whether to show labels or not"
    )]
    show_labels: Cell<bool>,
    marked_notes: RefCell<HashMap<u8, MarkedNoteStyle>>,
    /// The note pressed with the primary button
    pressed_note: Cell<u8>,
    priv_: RefCell<Priv>,
}

impl PianoWidgetPriv {
    fn note_pressed(&self, note: u8) {
        self.obj().emit_by_name::<()>("note-pressed", &[&note]);
    }

    fn note_released(&self, note: u8) {
        self.obj().emit_by_name::<()>("note-released", &[&note]);
    }

    fn point_to_midi(&self, x: f64, y: f64) -> Result<u8, PianoError> {
        //TODO Replace this by simple mathematical evaluation ?
        let p = self.priv_.borrow();
        let ak_sep: f64 = (p.accident_key_width * p.key_width) / 2.0;
        let ak_height: f64 = p.accident_key_height * p.piano_height;
        for i in self.first_note.get()..self.first_note.get() + self.key_count.get() {
            let delta_x = x - self.midi_to_x(i).unwrap_or(0.0);

            if delta_x < p.key_width {
                let curr_accident = midi_is_accident(i);
                let next_accident = if curr_accident {
                    midi_is_accident(i + 2)
                } else {
                    midi_is_accident(i + 1)
                };

                //Identifying regions
                return if delta_x < ak_sep && y < ak_height {
                    Ok(i)
                } else if delta_x > (p.key_width - ak_sep) && y < ak_height {
                    Ok(if curr_accident && next_accident {
                        i + 2
                    } else if (curr_accident || next_accident) && curr_accident != next_accident {
                        i + 1
                    } else {
                        i
                    })
                } else {
                    Ok(if curr_accident { i + 1 } else { i })
                };
            }
        }
        Err(PianoError::InvalidCoordinates)
    }

    fn midi_to_x(&self, midi_note: u8) -> Result<f64, PianoError> {
        let norm_key: i32 = midi_note as i32 - self.first_note.get() as i32;
        if norm_key < 0 || norm_key > (self.first_note.get() + self.key_count.get()) as i32 {
            return Err(PianoError::OutOfRangeNote);
        }
        let p = self.priv_.borrow();
        let mut x: f64 = p.piano_x;
        x += (norm_key / 12) as f64 * 7.0 * p.key_width;
        let mut last_was_nkey = false;
        for i in 0..=(norm_key % 12) {
            if last_was_nkey {
                // only increment x when the last key was white
                x += p.key_width;
            }
            last_was_nkey = !midi_is_accident(i.try_into().unwrap());
        }
        Ok(x)
    }

    fn get_natural_keys_count(&self) -> u8 {
        let complete_octaves: u8 = self.key_count.get() / 12;
        let incomplete_octave: u8 = self.key_count.get() % 12;
        let mut key_count: u8 = complete_octaves * 7; //7 natural-notes per octave
        let mut tmp_note;
        for i in 0..incomplete_octave {
            //iterating rem. keys
            tmp_note = (self.first_note.get() + i) % 12; //normalized offset
            if !midi_is_accident(tmp_note) {
                key_count += 1;
            }
        }
        key_count
    }

    fn calculate_dimensions(&self, cr: &cairo::Context) {
        let mut p = self.priv_.borrow_mut();
        let w = self.obj();
        p.width = w.allocated_width() as f64; //total width for the widget
        p.height = w.allocated_height() as f64; //total height for the widget
        if self.show_labels.get() {
            p.piano_height = (0.8 * p.height) - (2 * p.v_padding) as f64;
            cr.select_font_face(
                "monospace",
                cairo::FontSlant::Normal,
                cairo::FontWeight::Normal,
            );
            //TODO Fix this font size (Pango?)
            cr.set_font_size(0.05 * p.width);
        } else {
            p.piano_height = p.height - (2 * p.v_padding) as f64;
        }
        p.piano_width = p.width - (2 * p.h_padding) as f64;
        p.piano_x = p.h_padding as f64;
        p.piano_y = (p.height - p.piano_height) - p.v_padding as f64;
        p.nat_keys = self.get_natural_keys_count();
        p.key_width = p.piano_width / p.nat_keys as f64;
    }

    fn draw_natural_keys(&self, cr: &cairo::Context) {
        let p = self.priv_.borrow();
        let x = p.piano_x;
        let y = p.piano_y;

        print_on_err!(cr.save());

        // Drawing all natural keys
        for i in 0..p.nat_keys {
            cr.rectangle(
                x + (i as f64) * p.key_width,
                p.piano_y,
                p.key_width,
                p.piano_height,
            );
        }
        print_on_err!(cr.stroke_preserve());
        let colour = self.nat_key_colour.borrow();
        cr.set_source_rgba(
            colour.red().into(),
            colour.green().into(),
            colour.blue().into(),
            colour.alpha().into(),
        );
        print_on_err!(cr.fill_preserve());
        cr.set_source_rgba(0.0, 0.0, 0.0, 1.0);
        print_on_err!(cr.stroke());

        // Redrawing marked white keys
        for (key, value) in &*self.marked_notes.borrow() {
            if !midi_is_accident(*key) {
                if let Ok(x) = self.midi_to_x(*key) {
                    cr.rectangle(x, y, p.key_width, p.piano_height);
                    cr.set_source_rgba(
                        value.0.red().into(),
                        value.0.green().into(),
                        value.0.blue().into(),
                        value.0.alpha().into(),
                    );
                    print_on_err!(cr.fill_preserve());
                    cr.set_source_rgba(0.0, 0.0, 0.0, 1.0);
                    print_on_err!(cr.stroke());
                }
            }
        }

        print_on_err!(cr.restore());
    }

    fn draw_accident_keys(&self, cr: &cairo::Context) {
        let priv_ = self.priv_.borrow();
        let mut x = priv_.piano_x;
        let y = priv_.piano_y;
        let w = priv_.accident_key_width * priv_.key_width;
        let h = priv_.accident_key_height * priv_.piano_height;

        print_on_err!(cr.save());

        // Drawing all accidentals
        let colour = self.accident_key_colour.borrow();
        cr.set_source_rgba(
            colour.red().into(),
            colour.green().into(),
            colour.blue().into(),
            colour.alpha().into(),
        );
        for i in self.first_note.get()..self.first_note.get() + self.key_count.get() {
            if midi_is_accident(i) {
                cr.rectangle(x - w / 2.0, y, w, h);
            } else {
                x += priv_.key_width;
            }
        }
        print_on_err!(cr.fill_preserve());
        cr.set_source_rgba(0.0, 0.0, 0.0, 1.0);
        print_on_err!(cr.stroke());

        // Redrawing marked black keys
        for (key, value) in &*self.marked_notes.borrow() {
            if midi_is_accident(*key) {
                if let Ok(x) = self.midi_to_x(*key) {
                    cr.rectangle(x - w / 2.0, y, w, h);
                    cr.set_source_rgba(
                        value.0.red().into(),
                        value.0.green().into(),
                        value.0.blue().into(),
                        value.0.alpha().into(),
                    );
                    print_on_err!(cr.fill_preserve());
                    cr.set_source_rgba(0.0, 0.0, 0.0, 1.0);
                    print_on_err!(cr.stroke());
                }
            }
        }

        print_on_err!(cr.restore());
    }

    fn draw_func(widget: &gtk4::DrawingArea, cr: &cairo::Context, _: i32, _: i32) {
        let w = widget
            .downcast_ref::<PianoWidget>()
            .expect("Incorrect type");
        let priv_ = w.imp();
        priv_.calculate_dimensions(cr);
        priv_.draw_natural_keys(cr);
        priv_.draw_accident_keys(cr);
    }

    fn redraw(&self) {
        let w = self.obj();
        w.queue_draw();
    }

    fn press_event(&self, _gesture: &gtk4::GestureDrag, x: f64, y: f64) {
        let _note_pressed = self.point_to_midi(x, y).map(|m| {
            self.pressed_note.set(m);
            self.note_pressed(m);
        });
    }

    fn motion_event(&self, gesture: &gtk4::GestureDrag, offset_x: f64, offset_y: f64) {
        if let Some((x, y)) = gesture.start_point() {
            let _note_pressed = self.point_to_midi(x + offset_x, y + offset_y).map(|m| {
                let pressed_note = self.pressed_note.get();
                if pressed_note != m {
                    self.note_released(pressed_note);
                    self.pressed_note.set(m);
                    self.note_pressed(m);
                }
            });
        }
    }

    fn release_event(&self, gesture: &gtk4::GestureDrag, offset_x: f64, offset_y: f64) {
        if let Some((x, y)) = gesture.start_point() {
            let _note_pressed = self.point_to_midi(x + offset_x, y + offset_y).map(|m| {
                self.pressed_note.set(255);
                self.note_released(m);
            });
        }
    }

    fn find_positions(&self, _note: &str) -> Vec<u8> {
        vec![]
    }
}

#[glib::derived_properties]
impl ObjectImpl for PianoWidgetPriv {
    fn constructed(&self) {
        self.parent_constructed();

        let obj = self.obj();
        let motion = gtk4::GestureDrag::new();
        motion.connect_drag_update(glib::clone!(
            #[weak]
            obj,
            move |gesture, x, y| {
                obj.imp().motion_event(gesture, x, y);
            }
        ));
        motion.connect_drag_begin(glib::clone!(
            #[weak]
            obj,
            move |gesture, x, y| {
                obj.imp().press_event(gesture, x, y);
            }
        ));
        motion.connect_drag_end(glib::clone!(
            #[weak]
            obj,
            move |gesture, x, y| {
                obj.imp().release_event(gesture, x, y);
            }
        ));
        obj.add_controller(motion);

        let mut p = self.priv_.borrow_mut();
        p.nat_keys = self.get_natural_keys_count();

        let w = self.obj();

        w.set_draw_func(&Self::draw_func);
        w.set_size_request(MINIMUM_WIDTH, MINIMUM_HEIGHT);
        obj.connect_notify_local(None, |w, spec| match spec.name() {
            "key-count" | "first-note" => {
                w.imp().redraw();
            }
            _ => {}
        });
    }

    fn signals() -> &'static [Signal] {
        use once_cell::sync::Lazy;
        static SIGNALS: Lazy<Vec<Signal>> = Lazy::new(|| {
            vec![
                Signal::builder("note-pressed")
                    .param_types([<u8>::static_type()])
                    .run_last()
                    .build(),
                Signal::builder("note-released")
                    .param_types([<u8>::static_type()])
                    .run_last()
                    .build(),
            ]
        });
        SIGNALS.as_ref()
    }
}

#[glib::object_subclass]
impl ObjectSubclass for PianoWidgetPriv {
    const NAME: &'static str = "PianoWidget";
    type Type = PianoWidget;
    type ParentType = gtk4::DrawingArea;

    fn new() -> Self {
        Self {
            auto_update: Cell::new(true),
            key_count: Cell::new(DEFAULT_KEY_COUNT),
            first_note: Cell::new(DEFAULT_FIRST_NOTE),
            nat_key_colour: RefCell::new(gdk4::RGBA::WHITE),
            accident_key_colour: RefCell::new(gdk4::RGBA::BLACK),
            show_labels: Cell::new(false),
            marked_notes: RefCell::new(HashMap::new()),
            pressed_note: Cell::new(255),
            priv_: RefCell::new(Priv::new()),
        }
    }
}

impl DrawingAreaImpl for PianoWidgetPriv {}

impl WidgetImpl for PianoWidgetPriv {
    fn size_allocate(&self, width: i32, height: i32, baseline: i32) {
        let width = if width < MINIMUM_WIDTH {
            MINIMUM_WIDTH
        } else {
            width
        };
        let height = if height < MINIMUM_HEIGHT {
            MINIMUM_HEIGHT
        } else {
            height
        };
        self.parent_size_allocate(width, height, baseline);
    }
}

pub trait PianoWidgetExt {
    /// Mark the MIDI note `midi_note` with the `colour`.
    fn mark_midi(&self, midi_note: u8, colour: Option<gdk4::RGBA>);
    /// Unmark the MIDI note.
    fn unmark_midi(&self, midi_note: u8);

    /// Mark note with `colour`.
    fn mark_note(&self, note: &str, colour: Option<gdk4::RGBA>);
    /// Unmark note.
    fn unmark_note(&self, note: &str);

    /// Unmark all (clear)
    fn unmark_all(&self);

    /// Connect to signal note-pressed
    fn connect_note_pressed<F: Fn(&Self, u8) + 'static>(&self, f: F) -> glib::SignalHandlerId;
    /// Connect to signal note-released
    fn connect_note_released<F: Fn(&Self, u8) + 'static>(&self, f: F) -> glib::SignalHandlerId;
}

impl PianoWidgetExt for PianoWidget {
    /// Highlight a `midi_note` using `colour`.
    fn mark_midi(&self, midi_note: u8, colour: Option<gdk4::RGBA>) {
        let priv_ = self.imp();
        let colour = colour.unwrap_or_else(|| gdk4::RGBA::new(0.0, 0.0, 0.5, 1.0));
        priv_
            .marked_notes
            .borrow_mut()
            .insert(midi_note, MarkedNoteStyle(colour));
        if priv_.auto_update.get() {
            priv_.redraw();
        }
    }

    /// Unhighlight a `midi_note` back to default.
    fn unmark_midi(&self, midi_note: u8) {
        let priv_ = self.imp();
        if priv_.marked_notes.borrow_mut().remove(&midi_note).is_some() && priv_.auto_update.get() {
            priv_.redraw();
        }
    }

    /// Highlight a `note` by name with `colour`.
    fn mark_note(&self, note: &str, colour: Option<gdk4::RGBA>) {
        let priv_ = self.imp();
        let colour = colour.unwrap_or_else(|| gdk4::RGBA::new(0.0, 0.0, 0.5, 1.0));

        let old_auto_update = priv_.auto_update.get();

        let positions = priv_.find_positions(note);
        for i in positions {
            priv_
                .marked_notes
                .borrow_mut()
                .insert(i, MarkedNoteStyle(colour));
        }

        priv_.auto_update.set(old_auto_update);
        if priv_.auto_update.get() {
            priv_.redraw();
        }
    }

    /// Unhighlight a `note` by name.
    fn unmark_note(&self, note: &str) {
        let priv_ = self.imp();
        let old_auto_update = priv_.auto_update.get();

        let positions = priv_.find_positions(note);
        for i in positions {
            priv_.marked_notes.borrow_mut().remove(&i);
        }

        priv_.auto_update.set(old_auto_update);

        if priv_.auto_update.get() {
            priv_.redraw();
        }
    }

    /// Unhighlight all notes.
    fn unmark_all(&self) {
        let priv_ = self.imp();
        priv_.marked_notes.borrow_mut().clear();
        if priv_.auto_update.get() {
            priv_.redraw();
        }
    }

    /// Connect to the signal when a note is pressed down.
    fn connect_note_pressed<F: Fn(&Self, u8) + 'static>(&self, f: F) -> glib::SignalHandlerId {
        self.connect_closure(
            "note-pressed",
            true,
            glib::closure_local!(move |w, note| {
                f(&w, note);
            }),
        )
    }

    /// Connect to the signal when a note is released.
    fn connect_note_released<F: Fn(&Self, u8) + 'static>(&self, f: F) -> glib::SignalHandlerId {
        self.connect_closure(
            "note-released",
            true,
            glib::closure_local!(move |w, note| {
                f(&w, note);
            }),
        )
    }
}
