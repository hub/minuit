Instruments definition
======================


The file `src/instruments/presets.toml` contains the definition for the
(factory) preset instruments.

Each instrument has an `id`, a `name` and a `synth`.

`id` is a unique string
`name` is the user visible string to name the preset instrument.
`description` (optional) a long form description of the preset.
`synth` is the id of the synthesizer used for this instrument.
`ui` (optional) is the name of the UI file to instantiate (no extension).
`soundbank` (optional) the name of a soundbank. The nature of the soundbank is
specific to the synth. See below for how downloads are handled.

Depending on the value of `synth`, `params` is needed.

Synths
------

* `qwertone` is a simple sine wave synth. It has been lifted out of the Rust
application _qwertone_. There is not parameter to set.

* `fluidlite` is a SoundFont sample syhnthesizer using _fluidlite_. Some params
are required:
  * `soundfont`: the name of the soundfont file to be used as installed during
  during the build process OR as part of the soundbank.
  * `bank` (optional, default: "0"): the bank number to use for this preset
  * `preset` (optional, default: "0"): the preset number to use for this preset.
  * `infourl`, `author`, `license` (optional): metadata about the soundfont.

Soundbank download
==================

DRAFT

Instruments can carry big amount of data for their soundbanks (100MB to GBs of
audio samples).
In order to keep the source distribution and app binary small, but to ensure
that the instruments are up to date, a system to manage and download soundbanks
must be implemented.

Requirements are:
- versionned: we must ensure the specific version of the soundbank is downloaded
- shippable: we should be able to ship the whole library
- upgradable: we must be able to download a newer version of the soundbank if
the shipped version is read only

SFZinstrument use git to host and we could use that for SFZ based instruments
(at the time of writing there is no SFZ support in this application)

Integration

Each preset that needs a soundbank must declare it. And the soundbank
manager will make sure it present.

A sound bank is defined by:
* `id`: the string id of the soundbank. Several instruments can use the same
* `type`: `git`, `archive`, `file` define which form the soundbank is in.
`git` is a git repository, `archive` is an archive to be decompressed, while
`file` is a single file.
* `url`: the URL of the git repository or file
* `sha256`: the sha256 checksum for the file
* `commit`: the commit sha for git
* `name`: user displayed name for the sound bank
* `author`: author info
* `license`: a SPDX license
* `infourl`: the URL for information on the soundbank
* `info`: a user displayed text

The soundbank after download is in its own directory, that is either the git
repository, a directory containing the downloaded or the archive content.
If a preset references a file, it is relative to the top-level directory of
the soundbank.

Currently supported archive formats are `.zip` and `.tar.xz`. The tar unpacking
code doesn't support stripping components so you probably will need to
reference the full path.

Download flow
-------------

This is the overal flow for downloading soundbanks.

```mermaid
graph TD;
  A(Instrument selected) --> B{Soundbank available};
  B --> |No| C(Download);
  B --> |Yes| E(Sound available)
  C --> D{Success}
  D --> |Yes| E
  D --> |No| F(Send Error)
  E ==> G(Inst:<br>Event Received)
  G --> H{Is it current soundbank}
  H --> |Yes| I(Enable Instrument)
  H --> |No| J(Ignore)
```
